#include <vulkan/vulkan.h>

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "common.h"
#include "pipe.h"



size_t read_spirv_shader(const char *spirv_path, const uint32_t **bin) {
  size_t filesize, read_byte_count;
  FILE *spirv;

  spirv = fopen(spirv_path, "rb");
  if (!spirv) {
    fprintf(stderr, "Failed to open shader: %s\n", spirv_path);
    return 0;
  }
  fseek(spirv, 0l, SEEK_END);
  filesize = ftell(spirv);
  *bin = (const uint32_t *)malloc(filesize);
  fseek(spirv, 0l, SEEK_SET);
  read_byte_count = fread((void *)*bin, 4, filesize >> 2, spirv);
  if (read_byte_count != filesize >> 2) {
    fprintf(stderr, "Failed to read shader: %s\n", spirv_path);
    free((void *)*bin);
    fclose(spirv);
    return 0;
  }

  fclose(spirv);

  return filesize;
}

VkResult create_shader_module(VkDevice dev, VkShaderModule *shader, const char *path, const uint32_t **spirv) {
  size_t spirv_len;
  VkShaderModuleCreateInfo shader_info;
  VkResult ret;

  spirv_len = read_spirv_shader(path, spirv);
  if (spirv_len == 0 || *spirv == NULL) {
    return VK_ERROR_FORMAT_NOT_SUPPORTED;
  }
  shader_info = (VkShaderModuleCreateInfo){
    VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO, NULL, NO_FLAGS, spirv_len, (const uint32_t *)*spirv,
  };

  ret = vkCreateShaderModule(dev, &shader_info, NULL, shader);
  if (ret != VK_SUCCESS) {
    free((void *)*spirv);
    *spirv = NULL;
  }

  return ret;
}

VkResult komp_pipe_set_desc_set_layout(KomputePipeline *pipe, VkDevice dev, KomputeImage *im) {
  pipe->desc_set_layout_bindings = (VkDescriptorSetLayoutBinding[2]){
    (VkDescriptorSetLayoutBinding){ 0, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 1, VK_SHADER_STAGE_COMPUTE_BIT, &im->sampler },
    (VkDescriptorSetLayoutBinding){ 1, VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, 1, VK_SHADER_STAGE_COMPUTE_BIT, NULL },
  };
  pipe->desc_set_layout_info = (VkDescriptorSetLayoutCreateInfo){
    VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO, NULL, NO_FLAGS, 2, pipe->desc_set_layout_bindings,
  };

  return vkCreateDescriptorSetLayout(dev, &pipe->desc_set_layout_info, NULL, &pipe->desc_set_layout);
}

VkResult komp_pipe_create_compute_pipe(KomputePipeline *pipe, VkDevice dev, VkShaderModule shader) {
  int spec_values[2] = { 16, 16 };

  pipe->spec_map_entries = (VkSpecializationMapEntry[2]){};

  for (int i = 0; i < 2; i++) {
    pipe->spec_map_entries[i] = (VkSpecializationMapEntry){ i, i * sizeof(int), sizeof(int) };
  }

  pipe->spec_info = (VkSpecializationInfo){ 2, pipe->spec_map_entries, 2 * sizeof(int), (const void *)spec_values };
  pipe->comp_pipe_info = (VkComputePipelineCreateInfo){
    VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO,
    NULL,
    NO_FLAGS,
    (VkPipelineShaderStageCreateInfo){
      VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
      NULL,
      NO_FLAGS,
      VK_SHADER_STAGE_COMPUTE_BIT,
      shader,
      "main",
      &pipe->spec_info,
    },
    pipe->pipe_layout,
    VK_NULL_HANDLE,
    0,
  };

  return vkCreateComputePipelines(dev, VK_NULL_HANDLE, 1, &pipe->comp_pipe_info, NULL, &pipe->comp_pipe);
}

void komp_pipe_destroy(KomputePipeline *pipe, VkDevice dev) {
  if (pipe->comp_pipe != VK_NULL_HANDLE) {
    vkDestroyPipeline(dev, pipe->comp_pipe, NULL);
    pipe->comp_pipe = VK_NULL_HANDLE;
  }

  if (pipe->pipe_layout != VK_NULL_HANDLE) {
    vkDestroyPipelineLayout(dev, pipe->pipe_layout, NULL);
    pipe->pipe_layout = VK_NULL_HANDLE;
  }

  if (pipe->desc_set_layout != VK_NULL_HANDLE) {
    vkDestroyDescriptorSetLayout(dev, pipe->desc_set_layout, NULL);
    pipe->desc_set_layout = VK_NULL_HANDLE;
  }
}

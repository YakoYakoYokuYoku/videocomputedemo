#ifndef KOMP_DEVICE_H
#define KOMP_DEVICE_H



#include <vulkan/vulkan.h>

#include <stdbool.h>
#include <stdint.h>

#include "common.h"



/**
 * QueueIndex:
 * @compute_option: Existance of a compute family.
 * @compute_family: Queue index of a compute family.
 *
 * Queue index used for command buffer submitions.
 */
typedef struct _QueueIndex {
  YOption compute_option;
  uint32_t compute_family;
} QueueIndex;

/**
 * KomputeMem:
 * @bufs: Array of buffer handles.
 * @buf_sizes: Array of buffer sizes in bytes.
 * @buf_infos: Array of buffer creation parameters structures.
 * @dev_mem: Logical device memory handle.
 * @mem_props: Memory properties structure.
 * @mem_alloc_info: Memory allocation parameters structure.
 * @mem_size: Logical device memory size in bytes.
 * @buf_cnt: Number of buffer handles.
 * @mem_type_index: Memory type index.
 *
 * Device memory abstraction type for memory management with the device.
 */
typedef struct _KomputeMem {
  VkBuffer *bufs;
  VkDeviceSize *buf_sizes;
  VkDeviceSize *buf_offsets;
  VkBufferCreateInfo *buf_infos;
  VkDeviceMemory dev_mem;

  VkPhysicalDeviceMemoryProperties mem_props;
  VkMemoryAllocateInfo mem_alloc_info;

  VkDeviceSize mem_size;

  uint32_t buf_cnt;
  uint32_t mem_type_index;
} KomputeMem;



/**
 * check_physical_device:
 * @dev: A physical device handle.
 * @dev_feature_report: Pointer to a physical device features structure used for feature report.
 *
 * Checks if the @dev physical device is capable for compute operations and reports its features via the @dev_feature_report
 * structure.
 *
 *
 * Returns: On success `true` and `false` on failure.
 */
bool check_physical_device(VkPhysicalDevice dev, VkPhysicalDeviceFeatures *dev_feature_report);

/**
 * retrieve_physical_device:
 * @instance: A Vulkan runtime instance.
 * @dev_feature_report: Pointer to a physical device features structure used for feature report.
 *
 * Retrieves the best @dev physical device that is capable for compute operations and reports its features via the @dev_feature_report
 * structure.
 *
 *
 * Returns: A `VkPhysicalDevice` handle of the device or `VK_NULL_HANDLE` on failure.
 */
VkPhysicalDevice retrieve_physical_device(VkInstance instance, VkPhysicalDeviceFeatures *dev_features);

/**
 * discover_queue_families:
 * @dev: A physical device handle.
 *
 * Performs a discovery on the @dev physical device queue families.
 *
 *
 * Returns: A #QueueIndex indicating a queue family for compute operations.
 */
QueueIndex discover_queue_families(VkPhysicalDevice dev);

/**
 * create_device:
 * @phys_dev: A physical device handle.
 * @phys_dev_feature: Pointer to a physical device features structure used for feature enablement.
 * @queue_index: Index to the compute queue family.
 *
 * Creates a logical device @phys_dev physical device that is capable for compute operations with features from the
 * @phys_dev_feature structure.
 *
 *
 * Returns: A `VkDevice` handle of the device or `VK_NULL_HANDLE` on failure.
 */
VkDevice create_device(VkPhysicalDevice phys_dev, VkPhysicalDeviceFeatures *phys_dev_feature, QueueIndex index);

/**
 * komp_mem_instanciate:
 * @komp_mem: Pointer to a memory manager structure.
 * @phys_dev: A physical device handle.
 * @dev: A logical device handle.
 *
 * Instanciates a @komp_mem memory manager.
 *
 *
 * Returns: On success `true` and `false` on failure.
 */
VkResult komp_mem_instanciate(KomputeMem *komp_mem, VkPhysicalDevice phys_dev, VkDevice dev);

/**
 * komp_mem_bind_bufs:
 * @komp_mem: Pointer to a memory manager structure.
 * @dev: A logical device handle.
 *
 * Creates and binds all of the memory buffers in the @komp_mem memory manager.
 *
 *
 * Returns: On success `true` and `false` on failure.
 */
VkResult komp_mem_bind_bufs(KomputeMem *komp_mem, VkDevice dev);

/**
 * komp_mem_destroy:
 * @komp_mem: Pointer to a memory manager structure.
 * @dev: A logical device handle.
 *
 * Destroys a @komp_mem memory manager.
 */
void komp_mem_destroy(KomputeMem *komp_mem, VkDevice dev);

/**
 * komp_cmd_begin:
 * @cmd: A command buffer handle.
 * @dev: A logical device handle.
 * @cmd_pool: A command pool handle.
 *
 * Allocates on the @cmd_pool command pool and begins a @cmd command buffer.
 *
 *
 * Returns: On success `true` and `false` on failure.
 */
VkResult komp_cmd_begin(VkCommandBuffer *cmd, VkDevice dev, VkCommandPool cmd_pool);



#endif /* KOMP_DEVICE_H */

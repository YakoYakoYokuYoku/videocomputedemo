#include <libavcodec/avcodec.h>
#include <libavcodec/codec.h>
#include <libavcodec/codec_id.h>
#include <libavcodec/packet.h>
#include <libavformat/avformat.h>
#include <libavutil/avutil.h>
#include <libavutil/error.h>
#include <libavutil/log.h>
#include <libavutil/pixfmt.h>
#include <libswscale/swscale.h>
#include <stdio.h>
#include <vulkan/vulkan.h>
#include <vulkan/vulkan_core.h>

#include "common.h"
#include "snap.h"
#include "video.h"



/**
 * SECTION: video
 * @title: Video
 * @short_description: Video management for VulkanVideoCompute.
 *
 * This module provides a video management framework for VulkanVideoCompute consisting of muxing/demuxing, encoding/decoding and Vulkan
 * frame upload/download.
 */



FmtSpec vid_handler_obtain_fmt(enum AVPixelFormat fmt) {
  switch (fmt) {
    case AV_PIX_FMT_YUV420P:
      return (FmtSpec){ VK_FORMAT_G8_B8_R8_3PLANE_420_UNORM, true, true, 3 };

    case AV_PIX_FMT_YUV422P:
      return (FmtSpec){ VK_FORMAT_G8_B8_R8_3PLANE_422_UNORM, true, false, 3 };

    case AV_PIX_FMT_YUV444P:
      return (FmtSpec){ VK_FORMAT_G8_B8_R8_3PLANE_444_UNORM, false, false, 3 };

    case AV_PIX_FMT_RGBA:
      return (FmtSpec){ VK_FORMAT_R8G8B8A8_UNORM, false, false, 1 };

    default:
      break;
  }

  return (FmtSpec){ VK_FORMAT_UNDEFINED, false, false, 0 };
}

int vid_handler_open_dec_video(KomputeVideoHandler *vid_handler, const char *path) {
  AVCodec *cdc;
  int ret;

  vid_handler->frame = av_frame_alloc();
  vid_handler->pkt = av_packet_alloc();

  ret = avformat_open_input(&vid_handler->fmt_ctx, path, NULL, NULL);
  if (ret < 0) {
    return ret;
  }

  /* ret = avformat_find_stream_info(vid_handler->fmt_ctx, NULL);
  if (ret < 0) {
    return ret;
  } */

  vid_handler->streams = vid_handler->fmt_ctx->streams;
  vid_handler->stream_cnt = vid_handler->fmt_ctx->nb_streams;
  for (int s = 0; s < vid_handler->stream_cnt; s++) {
    if (vid_handler->streams[s]->codecpar->codec_type == AVMEDIA_TYPE_VIDEO) {
      vid_handler->video_idx = s;
      vid_handler->framerate = vid_handler->streams[s]->r_frame_rate;
      vid_handler->time_base = vid_handler->streams[s]->time_base;
    }
  }

  cdc = avcodec_find_decoder(vid_handler->streams[vid_handler->video_idx]->codecpar->codec_id);
  if (cdc == NULL) {
    return AVERROR_DECODER_NOT_FOUND;
  }

  vid_handler->cdc_ctx = avcodec_alloc_context3(cdc);
  if (vid_handler->cdc_ctx == NULL) {
    return AVERROR(ENOMEM);
  }

  ret = avcodec_open2(vid_handler->cdc_ctx, cdc, NULL);
  if (ret < 0) {
    return ret;
  }

  av_dump_format(vid_handler->fmt_ctx, 0, path, 0);

  return ret;
}

/* bool vid_handler_open_enc_video(KomputeVideoHandler *vid_handler, const char *path) {
  AVOutputFormat *out_fmt;
  AVStream *stream;
  AVCodec *cdc;
  int linesize;
  int lineheight;
  int ret;

  vid_handler->frame = av_frame_alloc();

  vid_handler->frame->format = AV_PIX_FMT_RGBA;

  vid_handler->frame->height = vid_handler->height;
  vid_handler->frame->width = vid_handler->width;

  vid_handler->cr_stride = 4 * vid_handler->width;
  vid_handler->cb_stride = (vid_handler->cr_stride & ~SIZE_MASK) + MACH_SIZE;
  vid_handler->y_stride = !(vid_handler->height & 0x01);
  lineheight = (vid_handler->height & 0x01) ? vid_handler->height + 1 : vid_handler->height;

  vid_handler->y = (uint8_t *)malloc(vid_handler->cb_stride * lineheight);
  vid_handler->frame->data[0] = (uint8_t *)vid_handler->y;
  vid_handler->frame->linesize[0] = vid_handler->cb_stride;

  out_fmt = av_guess_format("matroska", NULL, "video/matroska");
  if (out_fmt == NULL) {
    return false;
  }

  ret = avformat_alloc_output_context2(&vid_handler->fmt_ctx, out_fmt, NULL, NULL);
  if (ret < 0) {
    return false;
  }

  cdc = avcodec_find_encoder_by_name("librav1e");
  if (cdc == NULL) {
    cdc = avcodec_find_encoder(AV_CODEC_ID_AV1);
    if (cdc == NULL) {
      return false;
    }
  }

  stream = avformat_new_stream(vid_handler->fmt_ctx, NULL);
  if (stream == NULL) {
    return false;
  }

  stream->r_frame_rate = vid_handler->framerate;
  stream->avg_frame_rate = vid_handler->framerate;
  stream->time_base = av_inv_q(vid_handler->framerate);
  stream->codecpar->codec_id = cdc->id;
  stream->codecpar->codec_type = AVMEDIA_TYPE_VIDEO;
  stream->codecpar->width = vid_handler->width;
  stream->codecpar->height = vid_handler->height;

  vid_handler->cdc_ctx = avcodec_alloc_context3(cdc);
  if (vid_handler->cdc_ctx == NULL) {
    return false;
  }
  vid_handler->cdc_ctx->time_base = vid_handler->time_base;
  vid_handler->cdc_ctx->framerate = vid_handler->framerate;
  vid_handler->cdc_ctx->bit_rate = 200000;
  vid_handler->cdc_ctx->pix_fmt = vid_handler->pix_fmt;
  vid_handler->cdc_ctx->width = vid_handler->width;
  vid_handler->cdc_ctx->height = vid_handler->height;
  vid_handler->cdc_ctx->codec_type = AVMEDIA_TYPE_VIDEO;

  ret = avcodec_open2(vid_handler->cdc_ctx, cdc, NULL);
  if (ret < 0) {
    return false;
  }

  ret = avio_open2(&vid_handler->fmt_ctx->pb, path, AVIO_FLAG_WRITE, NULL, NULL);
  if (ret < 0) {
    return false;
  }

  ret = avformat_write_header(vid_handler->fmt_ctx, NULL);

  return true;
} */

int vid_handler_open_enc_video(KomputeVideoHandler *vid_handler, const char *path) {
  AVStream *stream;
  int ret;

  ret = avformat_alloc_output_context2(&vid_handler->fmt_ctx, NULL, NULL, path);
  if (ret < 0) {
    return ret;
  }

  vid_handler->video_idx = -1;
  vid_handler->stream_map = malloc(vid_handler->stream_cnt * sizeof(int32_t));
  for (int s = 0; s < vid_handler->stream_cnt; s++) {
    AVStream *c;
    enum AVMediaType type;

    type = vid_handler->streams[s]->codecpar->codec_type;
    if (type != AVMEDIA_TYPE_AUDIO && type != AVMEDIA_TYPE_VIDEO && type != AVMEDIA_TYPE_SUBTITLE) {
      vid_handler->stream_map[s] = -1;
      continue;
    }

    vid_handler->stream_map[s] = s;

    c = avformat_new_stream(vid_handler->fmt_ctx, NULL);
    if (c == NULL) {
      return AVERROR_STREAM_NOT_FOUND;
    }

    if (type == AVMEDIA_TYPE_VIDEO) {
      vid_handler->video_idx = vid_handler->fmt_ctx->nb_streams - 1;
      continue;
    }

    ret = avcodec_parameters_copy(c->codecpar, vid_handler->streams[s]->codecpar);
    if (ret < 0) {
      return ret;
    }
    ret = av_dict_copy(&c->metadata, vid_handler->streams[s]->metadata, NO_FLAGS);
    if (ret < 0) {
      return ret;
    }
  }

  if (vid_handler->video_idx == -1) {
    av_log(NULL, AV_LOG_WARNING, "No video stream found, remuxing.");
  }

  return ret;
}

int vid_handler_set_encoder_params(KomputeVideoHandler *vid_handler, const char *path) {
  AVStream *stream;
  AVCodec *cdc;
  AVDictionary *opt;
  char *speed, *enc_name;
  size_t enc_name_len;
  int ret;

  vid_handler->fmt_ctx->debug |= FF_FDEBUG_TS;

  cdc = avcodec_find_encoder_by_name("librav1e");
  if (cdc == NULL) {
    cdc = avcodec_find_encoder(AV_CODEC_ID_AV1);
    if (cdc == NULL) {
      return AVERROR_ENCODER_NOT_FOUND;
    }
  }

  vid_handler->frame = av_frame_alloc();
  vid_handler->pkt = av_packet_alloc();

  vid_handler->frame->format = vid_handler->pix_fmt;

  vid_handler->frame->width = vid_handler->width;
  vid_handler->frame->height = vid_handler->height;

  vid_handler->frame->sample_aspect_ratio = (AVRational){ 1, 1 };

  /* Align the frame to the best fit */
  vid_handler->frame->linesize[0] = (vid_handler->width & ~SIZE_MASK) + MACH_SIZE;
  vid_handler->frame->linesize[1] = (vid_handler->sectioned_u & ~SIZE_MASK) + MACH_SIZE;
  vid_handler->frame->linesize[2] = vid_handler->frame->linesize[1];
  vid_handler->plane = vid_handler->frame->linesize[0] * vid_handler->height;
  vid_handler->semiplane = vid_handler->frame->linesize[2] * vid_handler->sectioned_v;

  /* Allocate one luminance and two chrominance planes */
  vid_handler->frame->data[0] = (uint8_t *)malloc(vid_handler->plane + 2 * vid_handler->semiplane);
  vid_handler->frame->data[1] = (vid_handler->frame->data[0] + vid_handler->plane);
  vid_handler->frame->data[2] = (vid_handler->frame->data[1] + vid_handler->semiplane);

  vid_handler->pts = 0;

  /* Get RGBA to YUV pixel format conversion context */
  vid_handler->sws_ctx = sws_getContext(
    vid_handler->width, vid_handler->height, AV_PIX_FMT_RGBA, vid_handler->frame->width, vid_handler->frame->height, vid_handler->pix_fmt,
    SWS_BICUBIC, NULL, NULL, NULL);

  vid_handler->cdc_ctx = avcodec_alloc_context3(cdc);
  if (vid_handler->cdc_ctx == NULL) {
    return AVERROR(ENOMEM);
  }

  /* Set codec parameters accourdingly */
  vid_handler->cdc_ctx->time_base = av_inv_q(vid_handler->framerate);
  vid_handler->cdc_ctx->framerate = vid_handler->framerate;
  vid_handler->cdc_ctx->bit_rate = vid_handler->bitrate;
  vid_handler->cdc_ctx->gop_size = 12;

  vid_handler->cdc_ctx->pix_fmt = vid_handler->pix_fmt;

  vid_handler->cdc_ctx->width = vid_handler->width;
  vid_handler->cdc_ctx->height = vid_handler->height;

  vid_handler->cdc_ctx->sample_aspect_ratio = (AVRational){ 1, 1 };

  if (vid_handler->fmt_ctx->oformat->flags & AVFMT_GLOBALHEADER) {
    vid_handler->cdc_ctx->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;
  }

  /* Set encoder speed */
  speed = (char[9]){ EMPTY_STRUCT };
  sprintf(speed, "speed=%d", vid_handler->speed);
  opt = NULL;
  av_dict_set_int(&opt, "speed", vid_handler->speed, NO_FLAGS);
  av_dict_set(&opt, "rav1e-params", speed, NO_FLAGS);
  /* Copy the codec parameters onto the stream ones */
  stream = vid_handler->fmt_ctx->streams[vid_handler->video_idx];
  ret = avcodec_parameters_from_context(stream->codecpar, vid_handler->cdc_ctx);
  if (ret < 0) {
    return ret;
  }

  stream->r_frame_rate = vid_handler->framerate;
  stream->avg_frame_rate = vid_handler->framerate;
  stream->time_base = vid_handler->time_base;

  enc_name_len = sizeof(LIBAVCODEC_IDENT) + strlen(cdc->name) + 2;
  enc_name = av_mallocz(enc_name_len);
  strncpy(enc_name, LIBAVCODEC_IDENT " ", enc_name_len);
  strncat(enc_name, cdc->name, enc_name_len);
  av_dict_set(&stream->metadata, "encoder", enc_name, AV_DICT_DONT_STRDUP_VAL | AV_DICT_DONT_OVERWRITE);

  ret = avio_open2(&vid_handler->fmt_ctx->pb, path, AVIO_FLAG_WRITE, NULL, NULL);
  if (ret < 0) {
    return ret;
  }

  ret = avcodec_open2(vid_handler->cdc_ctx, cdc, &opt);
  av_dict_free(&opt);
  if (ret < 0) {
    return ret;
  }

  ret = avformat_write_header(vid_handler->fmt_ctx, NULL);
  if (ret < 0) {
    return ret;
  }

  av_dump_format(vid_handler->fmt_ctx, 0, path, 1);

  return true;
}

int vid_handler_receive_packet(KomputeVideoHandler *vid_handler) {
  int ret;

  ret = av_read_frame(vid_handler->fmt_ctx, vid_handler->pkt);
  if (ret < 0) {
    if (ret == AVERROR_EOF) {
      vid_handler->eof = true;
    }

    av_packet_unref(vid_handler->pkt);
    return ret;
  }

  return ret;
}

int vid_handler_copy_packet(KomputeVideoHandler *src_vid_handler, KomputeVideoHandler *dst_vid_handler) {
  int s;
  int ret;

  if (
    src_vid_handler->pkt->stream_index >= dst_vid_handler->stream_cnt ||
    dst_vid_handler->stream_map[src_vid_handler->pkt->stream_index] == -1) {
    av_packet_unref(src_vid_handler->pkt);
    return 0;
  } else if (src_vid_handler->pkt->stream_index == dst_vid_handler->video_idx) {
    return 0;
  }


  ret = av_interleaved_write_frame(dst_vid_handler->fmt_ctx, src_vid_handler->pkt);

  av_packet_unref(src_vid_handler->pkt);

  return (ret < 0) ? ret : 1;
}

int vid_handler_decode_frame(KomputeVideoHandler *vid_handler) {
  int ret;

  ret = avcodec_send_packet(vid_handler->cdc_ctx, vid_handler->pkt);
  if (ret < 0) {
    av_packet_unref(vid_handler->pkt);
    return ret;
  }

  ret = avcodec_receive_frame(vid_handler->cdc_ctx, vid_handler->frame);
  if (ret < 0) {
    av_packet_unref(vid_handler->pkt);
    return ret;
  }

  /* Get dimensions if unset */
  if (vid_handler->height == 0)
    vid_handler->height = vid_handler->frame->height;
  if (vid_handler->width == 0)
    vid_handler->width = vid_handler->frame->width;
  if (vid_handler->height != vid_handler->frame->height) {
    av_packet_unref(vid_handler->pkt);
    return false;
  }
  if (vid_handler->width != vid_handler->frame->width) {
    av_packet_unref(vid_handler->pkt);
    return false;
  }

  /* Get format if unset */
  if (vid_handler->fmt == VK_FORMAT_UNDEFINED) {
    FmtSpec fmt_spec;

    fmt_spec = vid_handler_obtain_fmt(vid_handler->frame->format);

    vid_handler->fmt = fmt_spec.fmt;
    if (vid_handler->fmt == VK_FORMAT_UNDEFINED) {
      av_packet_unref(vid_handler->pkt);
      return false;
    }

    vid_handler->sectioned_u = vid_handler->width;
    vid_handler->sectioned_v = vid_handler->height;
    if (fmt_spec.half_width)
      vid_handler->sectioned_u /= 2;
    if (fmt_spec.half_height)
      vid_handler->sectioned_v /= 2;

    vid_handler->plane = vid_handler->width * vid_handler->height;
    vid_handler->semiplane = vid_handler->sectioned_u * vid_handler->sectioned_v;
  }

  av_packet_unref(vid_handler->pkt);
  return true;
}

int vid_handler_encode_frame(KomputeVideoHandler *vid_handler) {
  int ret;

  vid_handler->frame->pts = vid_handler->pts;
  vid_handler->pts += 1;

  ret = avcodec_send_frame(vid_handler->cdc_ctx, vid_handler->eof ? NULL : vid_handler->frame);
  if (ret < 0) {
    av_packet_unref(vid_handler->pkt);
    return ret;
  }

  while (ret >= 0) {
    ret = avcodec_receive_packet(vid_handler->cdc_ctx, vid_handler->pkt);
    if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF) {
      break;
    } else if (ret < 0) {
      av_packet_unref(vid_handler->pkt);
      return ret;
    }

    vid_handler->pkt->stream_index = vid_handler->fmt_ctx->streams[0]->index;
    /* vid_handler->pkt->pts = av_rescale_q(vid_handler->pts, vid_handler->cdc_ctx->time_base, vid_handler->time_base);
    vid_handler->pkt->dts = av_rescale_q(vid_handler->pts, vid_handler->cdc_ctx->time_base, vid_handler->time_base);
    vid_handler->pkt->duration = av_rescale_q(1, vid_handler->cdc_ctx->time_base, vid_handler->time_base); */
    av_packet_rescale_ts(vid_handler->pkt, vid_handler->cdc_ctx->time_base, vid_handler->time_base);

    ret = av_interleaved_write_frame(vid_handler->fmt_ctx, vid_handler->pkt);
    if (ret < 0) {
      fprintf(stderr, __FILE__ ":%d, %s\n", __LINE__, av_err2str(ret));
      av_packet_unref(vid_handler->pkt);
      return ret;
    }

    av_packet_unref(vid_handler->pkt);
  }

  av_packet_unref(vid_handler->pkt);

  return true;
}

int vid_handler_submit_frame(
  KomputeVideoHandler *vid_handler, VkDevice dev, VkDeviceMemory dev_mem, VkDeviceSize buf_offset, VkDeviceSize buf_size) {
  uint8_t *payload;
  uint8_t *luma;
  uint8_t *chromb;
  uint8_t *chromr;
  uint8_t *alpha;
  const uint8_t *y;
  const uint8_t *cb;
  const uint8_t *cr;
  const uint8_t *a;
  VkResult ret;

  ret = vkMapMemory(dev, dev_mem, buf_offset, buf_size, NO_FLAGS, (void **)&payload);
  if (ret != VK_SUCCESS) {
    return ret;
  }

  luma = payload;
  chromb = payload + vid_handler->plane;
  chromr = payload + vid_handler->plane + vid_handler->semiplane;
  y = vid_handler->frame->data[0];
  cb = vid_handler->frame->data[1];
  cr = vid_handler->frame->data[2];
  a = vid_handler->frame->data[3];

  for (int i = 0; i < vid_handler->height; i++) {
    memcpy((void *)luma, (const void *)y, vid_handler->width);
    luma += vid_handler->width;
    y += vid_handler->frame->linesize[0];
  }
  for (int i = 0; i < vid_handler->sectioned_v; i++) {
    memcpy((void *)chromb, (const void *)cb, vid_handler->sectioned_u);
    memcpy((void *)chromr, (const void *)cr, vid_handler->sectioned_u);
    chromb += vid_handler->sectioned_u;
    chromr += vid_handler->sectioned_u;
    cb += vid_handler->frame->linesize[1];
    cr += vid_handler->frame->linesize[2];
  }
  if (vid_handler->frame->data[4] != NULL) {
    alpha = payload + vid_handler->plane + 2 * vid_handler->semiplane;

    for (int i = 0; i < vid_handler->height; i++) {
      memcpy((void *)alpha, (const void *)a, vid_handler->width);
      alpha += vid_handler->width;
      a += vid_handler->frame->linesize[3];
    }
  }

  vkUnmapMemory(dev, dev_mem);

  return ret;
}

int vid_handler_receive_frame(
  KomputeVideoHandler *vid_handler, VkDevice dev, VkDeviceMemory dev_mem, VkDeviceSize buf_offset, VkDeviceSize buf_size) {
  uint8_t *payload;
  uint8_t *luma;
  uint8_t *y;
  const uint8_t **src_slice;
  const int *src_stride;
  VkResult ret;

  ret = vkMapMemory(dev, dev_mem, buf_offset, buf_size, NO_FLAGS, (void **)&payload);
  if (ret != VK_SUCCESS) {
    return ret;
  }

  src_slice = (const uint8_t *[4]){ payload, NULL, NULL, NULL };
  src_stride = (const int[4]){ 4 * vid_handler->width, 0, 0, 0 };

  sws_scale(vid_handler->sws_ctx, src_slice, src_stride, 0, vid_handler->height, vid_handler->frame->data, vid_handler->frame->linesize);

  vkUnmapMemory(dev, dev_mem);

  return ret;
}

void vid_handler_cleanup(KomputeVideoHandler *vid_handler) {
  if (vid_handler->fmt_ctx->oformat != NULL && vid_handler->frame->data[0] != NULL) {
    free(vid_handler->frame->data[0]);
    vid_handler->frame->data[0] = NULL;
    vid_handler->frame->data[1] = NULL;
    vid_handler->frame->data[2] = NULL;
  }
  if (vid_handler->frame != NULL) {
    av_frame_free(&vid_handler->frame);
    vid_handler->frame = NULL;
  }
  if (vid_handler->pkt != NULL) {
    av_packet_free(&vid_handler->pkt);
    vid_handler->pkt = NULL;
  }
  if (vid_handler->sws_ctx != NULL) {
    sws_freeContext(vid_handler->sws_ctx);
    vid_handler->sws_ctx = NULL;
  }
  if (vid_handler->cdc_ctx != NULL) {
    avcodec_free_context(&vid_handler->cdc_ctx);
    vid_handler->cdc_ctx = NULL;
  }
  if (vid_handler->fmt_ctx != NULL) {
    if (vid_handler->fmt_ctx->iformat != NULL) {
      avformat_close_input(&vid_handler->fmt_ctx);
      vid_handler->fmt_ctx = NULL;
    } else if (vid_handler->fmt_ctx->oformat != NULL) {
      if (vid_handler->fmt_ctx->oformat->flags & AVFMT_NOFILE) {
        avio_close(vid_handler->fmt_ctx->pb);
      }
      avformat_free_context(vid_handler->fmt_ctx);
      vid_handler->fmt_ctx = NULL;
    }
  }
  if (vid_handler->stream_map != NULL) {
    free(vid_handler->stream_map);
  }
}

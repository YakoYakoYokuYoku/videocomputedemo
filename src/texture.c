#include <vulkan/vulkan.h>

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "common.h"
#include "texture.h"



/**
 * SECTION: texture
 * @title: Texture
 * @short_description: Texture management for VulkanVideoCompute.
 *
 * This module provides a basic Vulkan textures management framework for VulkanVideoCompute.
 */



VkResult komp_im_alloc(KomputeImage *komp_im, KomputeMem *komp_mem, VkDevice dev) {
  VkMemoryRequirements mem_req;
  VkMemoryAllocateInfo mem_alloc_info;
  uint32_t mem_type_index;
  VkResult ret;

  vkGetImageMemoryRequirements(dev, komp_im->im, &mem_req);
  mem_type_index = VK_MAX_MEMORY_TYPES;

  for (uint32_t i = 0; i < komp_mem->mem_props.memoryTypeCount; i++) {
    VkMemoryPropertyFlags flag_set;
    flag_set = komp_mem->mem_props.memoryTypes[i].propertyFlags;
    if ((mem_req.memoryTypeBits & (1 << i)) && (flag_set & VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT)) {
      mem_type_index = i;
      break;
    }
  }
  if (mem_type_index == VK_MAX_MEMORY_TYPES) {
    return VK_ERROR_OUT_OF_DEVICE_MEMORY;
  }

  mem_alloc_info = (VkMemoryAllocateInfo){
    VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
    NULL,
    mem_req.size,
    mem_type_index,
  };
  ret = vkAllocateMemory(dev, &mem_alloc_info, NULL, &komp_im->im_mem);
  if (ret != VK_SUCCESS) {
    return ret;
  }
  komp_im->im_size = mem_req.size;
  ret = vkBindImageMemory(dev, komp_im->im, komp_im->im_mem, komp_im->im_off);

  return ret;
}

VkResult komp_im_transition_im(
  KomputeImage *komp_im, KomputeMem *komp_mem, VkDevice dev, VkCommandPool cmd_pool, VkImageLayout src_layout, VkImageLayout dst_layout,
  QueueIndex queue_index) {
  VkCommandBuffer cmd_transfer;
  uint32_t buf_mem_barr_toggle;
  VkBufferMemoryBarrier buf_mem_barr;
  VkImageMemoryBarrier im_barr;
  VkSubmitInfo submit_info;
  VkQueue komp_queue;
  VkPipelineStageFlags src_stage;
  VkPipelineStageFlags dst_stage;
  VkAccessFlags src_access_mask;
  VkAccessFlags dst_access_mask;
  VkResult ret;

  buf_mem_barr_toggle = 0;

  if (src_layout == VK_IMAGE_LAYOUT_UNDEFINED && dst_layout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL) {
    src_access_mask = NO_FLAGS;
    dst_access_mask = VK_ACCESS_TRANSFER_WRITE_BIT;

    src_stage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
    dst_stage = VK_PIPELINE_STAGE_TRANSFER_BIT;
  } else if (src_layout == VK_IMAGE_LAYOUT_UNDEFINED && dst_layout == VK_IMAGE_LAYOUT_GENERAL) {
    src_access_mask = NO_FLAGS;
    dst_access_mask = VK_ACCESS_TRANSFER_WRITE_BIT;

    src_stage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
    dst_stage = VK_PIPELINE_STAGE_TRANSFER_BIT;
  } else if (src_layout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL && dst_layout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL) {
    src_access_mask = VK_ACCESS_TRANSFER_WRITE_BIT;
    dst_access_mask = VK_ACCESS_SHADER_READ_BIT;

    src_stage = VK_PIPELINE_STAGE_TRANSFER_BIT;
    dst_stage = VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT;
  } else if (src_layout == VK_IMAGE_LAYOUT_GENERAL && dst_layout == VK_IMAGE_LAYOUT_GENERAL) {
    src_access_mask = VK_ACCESS_TRANSFER_WRITE_BIT;
    dst_access_mask = VK_ACCESS_TRANSFER_WRITE_BIT;

    src_stage = VK_PIPELINE_STAGE_TRANSFER_BIT;
    dst_stage = VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT;
  } else if (src_layout == VK_IMAGE_LAYOUT_GENERAL && dst_layout == VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL) {
    src_access_mask = NO_FLAGS;
    dst_access_mask = VK_ACCESS_TRANSFER_READ_BIT;

    src_stage = VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT;
    dst_stage = VK_PIPELINE_STAGE_TRANSFER_BIT;

    buf_mem_barr_toggle = 0;
    buf_mem_barr = (VkBufferMemoryBarrier){
      VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER,
      NULL,
      NO_FLAGS,
      VK_ACCESS_TRANSFER_WRITE_BIT,
      VK_QUEUE_FAMILY_IGNORED,
      VK_QUEUE_FAMILY_IGNORED,
      komp_mem->bufs[1],
      0,
      komp_mem->buf_sizes[1],
    };
  } else if (src_layout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL && dst_layout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL) {
    src_access_mask = NO_FLAGS;
    dst_access_mask = VK_ACCESS_TRANSFER_WRITE_BIT;

    src_stage = VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT;
    dst_stage = VK_PIPELINE_STAGE_TRANSFER_BIT;
  } else if (src_layout == VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL && dst_layout == VK_IMAGE_LAYOUT_GENERAL) {
    src_access_mask = VK_ACCESS_TRANSFER_READ_BIT;
    dst_access_mask = VK_ACCESS_TRANSFER_WRITE_BIT;

    src_stage = VK_PIPELINE_STAGE_TRANSFER_BIT;
    dst_stage = VK_PIPELINE_STAGE_TRANSFER_BIT;
  } else {
    return VK_ERROR_UNKNOWN;
  }

  ret = komp_cmd_begin(&cmd_transfer, dev, cmd_pool);
  if (ret != VK_SUCCESS) {
    return ret;
  }

  im_barr = (VkImageMemoryBarrier){
    VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
    NULL,
    src_access_mask,
    dst_access_mask,
    src_layout,
    dst_layout,
    VK_QUEUE_FAMILY_IGNORED,
    VK_QUEUE_FAMILY_IGNORED,
    komp_im->im,
    (VkImageSubresourceRange){
      VK_IMAGE_ASPECT_COLOR_BIT,
      0,
      1,
      0,
      1,
    },
  };
  vkCmdPipelineBarrier(cmd_transfer, src_stage, dst_stage, NO_FLAGS, 0, NULL, buf_mem_barr_toggle, &buf_mem_barr, 1, &im_barr);

  ret = vkEndCommandBuffer(cmd_transfer);
  if (ret != VK_SUCCESS) {
    vkFreeCommandBuffers(dev, cmd_pool, 1, &cmd_transfer);
    return ret;
  }

  vkGetDeviceQueue(dev, queue_index.compute_family, 0, &komp_queue);
  submit_info = (VkSubmitInfo){
    VK_STRUCTURE_TYPE_SUBMIT_INFO, NULL, 0, NULL, NULL, 1, &cmd_transfer, 0, NULL,
  };
  ret = vkQueueSubmit(komp_queue, 1, &submit_info, VK_NULL_HANDLE);
  if (ret != VK_SUCCESS) {
    vkFreeCommandBuffers(dev, cmd_pool, 1, &cmd_transfer);
    return ret;
  }

  ret = vkQueueWaitIdle(komp_queue);
  if (ret != VK_SUCCESS) {
    vkFreeCommandBuffers(dev, cmd_pool, 1, &cmd_transfer);
    return ret;
  }

  return ret;
}

VkResult komp_im_copy_buf_to_yuv_im(KomputeImage *komp_im, VkBuffer src_buf, VkDevice dev, VkCommandPool cmd_pool, QueueIndex queue_index) {
  VkCommandBuffer cmd_copy;
  VkBufferImageCopy *im_buf_copy;
  VkSubmitInfo submit_info;
  VkQueue komp_queue;
  VkResult ret;

  komp_cmd_begin(&cmd_copy, dev, cmd_pool);
  im_buf_copy = (VkBufferImageCopy[3]){
    (VkBufferImageCopy){
      0,
      0,
      0,
      (VkImageSubresourceLayers){
        VK_IMAGE_ASPECT_PLANE_0_BIT,
        0,
        0,
        1,
      },
      (VkOffset3D){ 0, 0, 0 },
      (VkExtent3D){ komp_im->width, komp_im->height, 1 },
    },
    (VkBufferImageCopy){
      komp_im->plane,
      0,
      0,
      (VkImageSubresourceLayers){
        VK_IMAGE_ASPECT_PLANE_1_BIT,
        0,
        0,
        1,
      },
      (VkOffset3D){ 0, 0, 0 },
      (VkExtent3D){ komp_im->sectioned_u, komp_im->sectioned_v, 1 },
    },
    (VkBufferImageCopy){
      komp_im->plane + komp_im->semiplane,
      0,
      0,
      (VkImageSubresourceLayers){
        VK_IMAGE_ASPECT_PLANE_2_BIT,
        0,
        0,
        1,
      },
      (VkOffset3D){ 0, 0, 0 },
      (VkExtent3D){ komp_im->sectioned_u, komp_im->sectioned_v, 1 },
    },
  };
  vkCmdCopyBufferToImage(cmd_copy, src_buf, komp_im->im, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 3, im_buf_copy);

  ret = vkEndCommandBuffer(cmd_copy);
  if (ret != VK_SUCCESS) {
    vkFreeCommandBuffers(dev, cmd_pool, 1, &cmd_copy);
    return ret;
  }

  vkGetDeviceQueue(dev, queue_index.compute_family, 0, &komp_queue);
  submit_info = (VkSubmitInfo){
    VK_STRUCTURE_TYPE_SUBMIT_INFO, NULL, 0, NULL, NULL, 1, &cmd_copy, 0, NULL,
  };
  ret = vkQueueSubmit(komp_queue, 1, &submit_info, VK_NULL_HANDLE);
  if (ret != VK_SUCCESS) {
    vkFreeCommandBuffers(dev, cmd_pool, 1, &cmd_copy);
    return ret;
  }

  ret = vkQueueWaitIdle(komp_queue);
  vkFreeCommandBuffers(dev, cmd_pool, 1, &cmd_copy);

  return ret;
}

VkResult komp_im_copy_rgb_im_to_buf(KomputeImage *komp_im, VkBuffer dst_buf, VkDevice dev, VkCommandPool cmd_pool, QueueIndex queue_index) {
  VkCommandBuffer cmd_copy;
  VkBufferImageCopy im_buf_copy;
  VkSubmitInfo submit_info;
  VkQueue komp_queue;
  VkResult ret;

  komp_cmd_begin(&cmd_copy, dev, cmd_pool);
  im_buf_copy = (VkBufferImageCopy){
    0,
    0,
    0,
    (VkImageSubresourceLayers){
      VK_IMAGE_ASPECT_COLOR_BIT,
      0,
      0,
      1,
    },
    (VkOffset3D){ 0, 0, 0 },
    (VkExtent3D){ komp_im->width, komp_im->height, 1 },
  };
  vkCmdCopyImageToBuffer(cmd_copy, komp_im->im, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, dst_buf, 1, &im_buf_copy);

  ret = vkEndCommandBuffer(cmd_copy);
  if (ret != VK_SUCCESS) {
    vkFreeCommandBuffers(dev, cmd_pool, 1, &cmd_copy);
    return ret;
  }

  vkGetDeviceQueue(dev, queue_index.compute_family, 0, &komp_queue);
  submit_info = (VkSubmitInfo){
    VK_STRUCTURE_TYPE_SUBMIT_INFO, NULL, 0, NULL, NULL, 1, &cmd_copy, 0, NULL,
  };
  ret = vkQueueSubmit(komp_queue, 1, &submit_info, VK_NULL_HANDLE);
  if (ret != VK_SUCCESS) {
    vkFreeCommandBuffers(dev, cmd_pool, 1, &cmd_copy);
    return ret;
  }

  ret = vkQueueWaitIdle(komp_queue);
  vkFreeCommandBuffers(dev, cmd_pool, 1, &cmd_copy);


  return ret;
}

VkResult komp_im_create_im_view_ycbcr(KomputeImage *komp_im, VkDevice dev) {
  VkResult ret;

  komp_im->ycbcr_conv_info = (VkSamplerYcbcrConversionCreateInfo){
    VK_STRUCTURE_TYPE_SAMPLER_YCBCR_CONVERSION_CREATE_INFO,
    NULL,
    komp_im->fmt,
    VK_SAMPLER_YCBCR_MODEL_CONVERSION_YCBCR_601,
    VK_SAMPLER_YCBCR_RANGE_ITU_FULL,
    (VkComponentMapping){
      VK_COMPONENT_SWIZZLE_IDENTITY,
      VK_COMPONENT_SWIZZLE_IDENTITY,
      VK_COMPONENT_SWIZZLE_IDENTITY,
      VK_COMPONENT_SWIZZLE_IDENTITY,
    },
    VK_CHROMA_LOCATION_MIDPOINT,
    VK_CHROMA_LOCATION_MIDPOINT,
    VK_FILTER_NEAREST,
    VK_FALSE,
  };
  ret = vkCreateSamplerYcbcrConversion(dev, &komp_im->ycbcr_conv_info, NULL, &komp_im->ycbcr_conv);
  if (ret != VK_SUCCESS)
    return ret;

  komp_im->ycbcr_sampler_info = (VkSamplerYcbcrConversionInfo){
    VK_STRUCTURE_TYPE_SAMPLER_YCBCR_CONVERSION_INFO,
    NULL,
    komp_im->ycbcr_conv,
  };
  komp_im->im_view_info = (VkImageViewCreateInfo){
    VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
    &komp_im->ycbcr_sampler_info,
    NO_FLAGS,
    komp_im->im,
    VK_IMAGE_VIEW_TYPE_2D,
    komp_im->fmt,
    (VkComponentMapping){
      VK_COMPONENT_SWIZZLE_IDENTITY,
      VK_COMPONENT_SWIZZLE_IDENTITY,
      VK_COMPONENT_SWIZZLE_IDENTITY,
      VK_COMPONENT_SWIZZLE_IDENTITY,
    },
    (VkImageSubresourceRange){ VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1 },
  };
  ret = vkCreateImageView(dev, &komp_im->im_view_info, NULL, &komp_im->im_view);

  return ret;
}

VkResult komp_im_create_im_view_rgba8(KomputeImage *komp_im, VkDevice dev) {
  VkResult ret;

  komp_im->im_view_info = (VkImageViewCreateInfo){
    VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
    NULL,
    NO_FLAGS,
    komp_im->im,
    VK_IMAGE_VIEW_TYPE_2D,
    komp_im->fmt,
    (VkComponentMapping){
      VK_COMPONENT_SWIZZLE_IDENTITY,
      VK_COMPONENT_SWIZZLE_IDENTITY,
      VK_COMPONENT_SWIZZLE_IDENTITY,
      VK_COMPONENT_SWIZZLE_IDENTITY,
    },
    (VkImageSubresourceRange){ VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1 },
  };
  ret = vkCreateImageView(dev, &komp_im->im_view_info, NULL, &komp_im->im_view);

  return ret;
}

VkResult komp_im_create_sampler_ycbcr(KomputeImage *komp_im, VkDevice dev) {
  VkSamplerCreateInfo sampler_info;
  VkResult ret;

  sampler_info = (VkSamplerCreateInfo){
    VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO,
    &komp_im->ycbcr_sampler_info,
    NO_FLAGS,
    VK_FILTER_NEAREST,
    VK_FILTER_NEAREST,
    VK_SAMPLER_MIPMAP_MODE_NEAREST,
    VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
    VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
    VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
    0.0f,
    VK_FALSE,
    0.0f,
    VK_FALSE,
    VK_COMPARE_OP_ALWAYS,
    0.0f,
    0.0f,
    VK_BORDER_COLOR_INT_OPAQUE_BLACK,
    VK_FALSE,
  };
  ret = vkCreateSampler(dev, &sampler_info, NULL, &komp_im->sampler);

  return ret;
}

void komp_im_destroy(KomputeImage *komp_im, VkDevice dev) {
  if (komp_im->sampler != VK_NULL_HANDLE) {
    vkDestroySampler(dev, komp_im->sampler, NULL);
    komp_im->sampler = VK_NULL_HANDLE;
  }
  if (komp_im->ycbcr_conv != VK_NULL_HANDLE) {
    vkDestroySamplerYcbcrConversion(dev, komp_im->ycbcr_conv, NULL);
    komp_im->ycbcr_conv = VK_NULL_HANDLE;
  }
  if (komp_im->im_view != VK_NULL_HANDLE) {
    vkDestroyImageView(dev, komp_im->im_view, NULL);
    komp_im->im_view = VK_NULL_HANDLE;
  }
  if (komp_im->im != VK_NULL_HANDLE) {
    vkDestroyImage(dev, komp_im->im, NULL);
    komp_im->im = VK_NULL_HANDLE;
  }
  if (komp_im->im_mem != VK_NULL_HANDLE) {
    vkFreeMemory(dev, komp_im->im_mem, NULL);
    komp_im->im_mem = VK_NULL_HANDLE;
  }
}

#include <libavutil/log.h>
#include <libavutil/parseutils.h>
#include <vulkan/vulkan.h>

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "common.h"
#include "device.h"
#include "pipe.h"
#include "texture.h"
#include "video.h"



static const char *HELP_STR =
  "vulkanvideocompute [-speed <0-10>] [-shader <path to SPIRV binary>] [-bitrate <bitrate>] <input-video>.* <output-video>.mkv\n"
  "\n"
  "    -speed <0-10>: Speed of the rav1e encoder from 0 to 10.\n"
  "    -shader <path to SPIRV binary>: Compute shader to be used (see texture.spv).\n"
  "    -bitrate <bitrate>: Bitrate of the encoded video in bits, can be suffixed with k (kilos) and M (megas).\n"
  "\n"
  "    -help: Show this help message.\n"
  "\n"
  "    <input-video>.*: Input video with both container and codec formats supported by FFmpeg.\n"
  "    <output-video>.*: Output video encoded in AV1 contained in a format that supports this codec.";



typedef struct komputeapp {
  KomputeMem komp_mem;
  KomputeImage komp_im_in;
  KomputeImage komp_im_out;

  KomputePipeline komp_pipe;

  QueueIndex index;

  KomputeVideoHandler dec_vid_handler;
  KomputeVideoHandler enc_vid_handler;

  VkDescriptorPoolSize *desc_pool_sizes;

  float *payload;

  VkInstance instance;

  VkPhysicalDevice phys_dev;
  VkDevice dev;

  const uint32_t *compute_spirv;
  VkShaderModule compute_shader;

  VkDescriptorPool desc_pool;
  VkDescriptorSet desc_set;
  VkCommandPool cmd_pool;
  VkCommandBuffer cmd_run;
} KomputeApplication;



char *komp_app_error(const char *err_str, int *err_len) {
  int str_len;
  char *ret;

  str_len = strnlen(err_str, INT32_MAX);
  if (str_len == 0) {
    return NULL;
  }
  if (err_len != NULL) {
    *err_len = str_len;
  }

  ret = (char *)malloc(str_len);
  memcpy(ret, err_str, str_len);

  return ret;
}

void komp_print_error(char *err_str, int err_len, int reason) {
  fprintf(stderr, "ERROR! %.*s", err_len, err_str);
  if (reason > 0) {
    fprintf(stderr, " - Vulkan Result code: %d\n", reason);
  } else if (reason < 0) {
    fprintf(stderr, " - FFmpeg Failure: %s\n", av_err2str(reason));
  }
}

void komp_print_help() {
  printf("%s\n", HELP_STR);
}

void komp_app_destroy(KomputeApplication *komp_app) {
  if (komp_app->cmd_run != VK_NULL_HANDLE) {
    vkFreeCommandBuffers(komp_app->dev, komp_app->cmd_pool, 1, &komp_app->cmd_run);
  }
  if (komp_app->cmd_pool != VK_NULL_HANDLE) {
    vkDestroyCommandPool(komp_app->dev, komp_app->cmd_pool, NULL);
  }
  if (komp_app->desc_set != VK_NULL_HANDLE) {
    vkFreeDescriptorSets(komp_app->dev, komp_app->desc_pool, 1, &komp_app->desc_set);
  }
  if (komp_app->desc_pool != VK_NULL_HANDLE) {
    vkDestroyDescriptorPool(komp_app->dev, komp_app->desc_pool, NULL);
  }
  if (komp_app->komp_pipe.pipe_layout != VK_NULL_HANDLE) {
    komp_pipe_destroy(&komp_app->komp_pipe, komp_app->dev);
  }
  if (komp_app->compute_shader != VK_NULL_HANDLE) {
    vkDestroyShaderModule(komp_app->dev, komp_app->compute_shader, NULL);
  }
  if (komp_app->compute_spirv != NULL) {
    free((void *)komp_app->compute_spirv);
  }
  if (komp_app->komp_im_out.im != VK_NULL_HANDLE) {
    komp_im_destroy(&komp_app->komp_im_out, komp_app->dev);
  }
  if (komp_app->komp_im_in.im != VK_NULL_HANDLE) {
    komp_im_destroy(&komp_app->komp_im_in, komp_app->dev);
  }
  if (komp_app->komp_mem.dev_mem != VK_NULL_HANDLE) {
    komp_mem_destroy(&komp_app->komp_mem, komp_app->dev);
  }
  if (komp_app->dev != VK_NULL_HANDLE) {
    vkDestroyDevice(komp_app->dev, NULL);
  }
  if (komp_app->instance != VK_NULL_HANDLE) {
    vkDestroyInstance(komp_app->instance, NULL);
  }
  if (komp_app->dec_vid_handler.fmt_ctx != NULL) {
    vid_handler_cleanup(&komp_app->dec_vid_handler);
  }
  if (komp_app->enc_vid_handler.fmt_ctx != NULL) {
    vid_handler_cleanup(&komp_app->enc_vid_handler);
  }
}


int main(int argc, const char **argv) {
  KomputeApplication komp_app;

  VkApplicationInfo app_info;
  VkInstanceCreateInfo instance_info;

  VkPhysicalDeviceFeatures phys_dev_features;

  uint8_t *payload, *luma, *chromb, *chromr;

  VkImageFormatProperties yuv_420_im_fmt_prop, yuv_422_im_fmt_prop, yuv_444_im_fmt_prop;

  VkDescriptorPoolCreateInfo desc_pool_info;
  VkDescriptorSetAllocateInfo desc_set_alloc_info;
  VkDescriptorBufferInfo desc_buf_info_im_in, desc_buf_info_im_out;
  VkDescriptorImageInfo desc_buf_im_in, desc_buf_im_out;
  VkWriteDescriptorSet *write_desc_set;
  VkCommandPoolCreateInfo cmd_pool_info;

  VkQueue compute_queue;
  VkSubmitInfo compute_submit_info;

  PushParam param;

  const char *validation_layer_str;
  uint32_t validation_layer_on;

  const char *spirv_path;

  const char *in_uri;

  int o;
  uint8_t speed;
  AVRational bitrate;

  int ret, err, err_len;
  char *err_str;


  speed = 0;
  bitrate = av_make_q(200000, 1);
  for (o = 1; o < argc - 2; o++) {
    if (strncmp(argv[o], "-help", 6) == 0) {
      komp_print_help();
      return 0;
    }

    if (strncmp(argv[o], "-speed", 7) == 0) {
      speed = (uint8_t)MIN(strtoul(argv[o + 1], NULL, 10), 10);
      o++;
      continue;
    }

    if (strncmp(argv[o], "-shader", 8) == 0) {
      spirv_path = argv[o + 1];
      o++;
      continue;
    }

    if (strncmp(argv[o], "-bitrate", 9) == 0) {
      if (av_parse_ratio(&bitrate, argv[o + 1], 400000000, 0, NULL) < 0) {
        bitrate = av_make_q(200000, 1);
      }
      o++;
      continue;
    }
  }

  /* printf("%d %d\n", o, speed);
  printf("%d/%d == %ld\n", bitrate.num, bitrate.den, av_rescale(1, bitrate.num, bitrate.den)); */
  speed = speed == 0 ? 6 : speed;
  if (bitrate.num == 0 || bitrate.den == 0 || bitrate.num <= bitrate.den) {
    fprintf(stderr, "WARNING! Erroneous bitrate, setting to 200k.\n");
    bitrate = av_make_q(200000, 1);
  }
  if (argv[o] == NULL || argv[o + 1] == NULL) {
    fprintf(stderr, "ERROR! Provide an input and output video files.\n");
    komp_print_help();
    return 1;
  }


  ret = 0;
  err = VK_SUCCESS;
  err_str = NULL;
  err_len = 0;

  av_log_set_level(AV_LOG_INFO);

  validation_layer_on = getenv("VAL_LAYERS") == NULL ? 0 : 1;
  validation_layer_str = "VK_LAYER_KHRONOS_validation";

  if (spirv_path == NULL || spirv_path[0] == '\0') {
    spirv_path = getenv("SPIRV_PATH");
    if (spirv_path == NULL || spirv_path[0] == '\0') {
      spirv_path = "texture.spv";
    }
  }

  komp_app = (KomputeApplication){ EMPTY_STRUCT };

  app_info = (VkApplicationInfo){
    VK_STRUCTURE_TYPE_APPLICATION_INFO,
    NULL,
    "Video Compute",
    VK_MAKE_VERSION(1, 2, 0),
    "No Engine",
    VK_MAKE_VERSION(1, 2, 0),
    VK_API_VERSION_1_2,
  };
  instance_info = (VkInstanceCreateInfo){
    VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO, NULL, NO_FLAGS, &app_info, validation_layer_on, &validation_layer_str, 0, NULL,
  };
  err = vkCreateInstance(&instance_info, NULL, &komp_app.instance);
  if (err != VK_SUCCESS) {
    err_str = "Couldn't create Vulkan Instance";
    komp_print_error(err_str, strlen(err_str), err);
    return 1;
  }

  komp_app.phys_dev = retrieve_physical_device(komp_app.instance, &phys_dev_features);
  if (komp_app.phys_dev == VK_NULL_HANDLE) {
    err_str = "No Vulkan devices available";
    komp_print_error(err_str, strlen(err_str), err);
    komp_app_destroy(&komp_app);
    return 1;
  }

  /* Retrieve YUV capabilities from the device */
  err = vkGetPhysicalDeviceImageFormatProperties(
    komp_app.phys_dev, VK_FORMAT_G8_B8_R8_3PLANE_420_UNORM, VK_IMAGE_TYPE_2D, VK_IMAGE_TILING_OPTIMAL,
    VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_SAMPLED_BIT, NO_FLAGS, &yuv_420_im_fmt_prop);
  if (err != VK_SUCCESS) {
    err_str = "Failed to get YUV 420 image format properties";
    komp_print_error(err_str, strlen(err_str), err);
    komp_app_destroy(&komp_app);
    return 1;
  }
  err = vkGetPhysicalDeviceImageFormatProperties(
    komp_app.phys_dev, VK_FORMAT_G8_B8_R8_3PLANE_422_UNORM, VK_IMAGE_TYPE_2D, VK_IMAGE_TILING_OPTIMAL,
    VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_SAMPLED_BIT, NO_FLAGS, &yuv_422_im_fmt_prop);
  if (err != VK_SUCCESS) {
    err_str = "Failed to get YUV 422 image format properties";
    komp_print_error(err_str, strlen(err_str), err);
    komp_app_destroy(&komp_app);
    return 1;
  }
  err = vkGetPhysicalDeviceImageFormatProperties(
    komp_app.phys_dev, VK_FORMAT_G8_B8_R8_3PLANE_444_UNORM, VK_IMAGE_TYPE_2D, VK_IMAGE_TILING_OPTIMAL,
    VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_SAMPLED_BIT, NO_FLAGS, &yuv_444_im_fmt_prop);
  if (err != VK_SUCCESS) {
    err_str = "Failed to get YUV 444 image format properties";
    komp_print_error(err_str, strlen(err_str), err);
    komp_app_destroy(&komp_app);
    return 1;
  }

  komp_app.index = discover_queue_families(komp_app.phys_dev);
  if (komp_app.index.compute_option == NONE) {
    err_str = "No compute queue family found";
    komp_print_error(err_str, strlen(err_str), err);
    komp_app_destroy(&komp_app);
    return 1;
  }

  komp_app.dev = create_device(komp_app.phys_dev, &phys_dev_features, komp_app.index);
  if (komp_app.dev == VK_NULL_HANDLE) {
    err_str = "Failed to create logical device";
    komp_print_error(err_str, strlen(err_str), err);
    komp_app_destroy(&komp_app);
    return 1;
  }

  cmd_pool_info = (VkCommandPoolCreateInfo){
    VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
    NULL,
    NO_FLAGS,
    komp_app.index.compute_family,
  };
  err = vkCreateCommandPool(komp_app.dev, &cmd_pool_info, NULL, &komp_app.cmd_pool);
  if (err != VK_SUCCESS) {
    err_str = "Command pool couldn't be created";
    komp_print_error(err_str, strlen(err_str), err);
    komp_app_destroy(&komp_app);
    return 1;
  }

  err = create_shader_module(komp_app.dev, &komp_app.compute_shader, spirv_path, &komp_app.compute_spirv);
  if (err != VK_SUCCESS) {
    err_str = "Failed to create shader module";
    komp_print_error(err_str, strlen(err_str), err);
    komp_app_destroy(&komp_app);
    return 1;
  }

  komp_app.desc_pool_sizes = (VkDescriptorPoolSize[2]){
    (VkDescriptorPoolSize){
      VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
      1,
    },
    (VkDescriptorPoolSize){
      VK_DESCRIPTOR_TYPE_STORAGE_IMAGE,
      1,
    },
  };
  desc_pool_info = (VkDescriptorPoolCreateInfo){
    VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO, NULL, VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT, 1, 2, komp_app.desc_pool_sizes,
  };
  err = vkCreateDescriptorPool(komp_app.dev, &desc_pool_info, NULL, &komp_app.desc_pool);
  if (err != VK_SUCCESS) {
    err_str = "Failed to create descriptor pool";
    komp_print_error(err_str, strlen(err_str), err);
    komp_app_destroy(&komp_app);
    return 1;
  }

  komp_app.dec_vid_handler = (KomputeVideoHandler){ EMPTY_STRUCT };
  err = vid_handler_open_dec_video(&komp_app.dec_vid_handler, argv[o]);
  if (err < 0) {
    err_str = "Failed to create input video handler";
    komp_print_error(err_str, strlen(err_str), err);
    komp_app_destroy(&komp_app);
    return 1;
  }

  komp_app.enc_vid_handler = (KomputeVideoHandler){ EMPTY_STRUCT };
  komp_app.enc_vid_handler.streams = komp_app.dec_vid_handler.streams;
  komp_app.enc_vid_handler.stream_cnt = komp_app.dec_vid_handler.stream_cnt;

  err = vid_handler_open_enc_video(&komp_app.enc_vid_handler, argv[o]);
  if (err < 0) {
    err_str = "Failed to create output video handler";
    komp_print_error(err_str, strlen(err_str), err);
    komp_app_destroy(&komp_app);
    return 1;
  }

  komp_app.komp_mem.bufs = (VkBuffer[2]){ VK_NULL_HANDLE, VK_NULL_HANDLE };
  komp_app.komp_mem.buf_sizes = (VkDeviceSize[2]){ 0, 0 };
  komp_app.komp_mem.buf_offsets = (VkDeviceSize[2]){ 0, 0 };
  komp_app.komp_mem.buf_infos = (VkBufferCreateInfo[2]){ (VkBufferCreateInfo){ EMPTY_STRUCT }, (VkBufferCreateInfo){ EMPTY_STRUCT } };

  while (true) {
    err = vid_handler_receive_packet(&komp_app.dec_vid_handler);
    if (err < 0) {
      if (!komp_app.dec_vid_handler.eof) {
        err_str = komp_app_error("Failed to receive packet from demuxer", &err_len);
      } else {
        err = 0;
      }
      break;
    }

    err = vid_handler_copy_packet(&komp_app.dec_vid_handler, &komp_app.enc_vid_handler);
    if (err < 0) {
      err_str = komp_app_error("Failed to copy packet from source video handler to destination video handler", &err_len);
      break;
    } else if (err == 1) {
      continue;
    }

    err = vid_handler_decode_frame(&komp_app.dec_vid_handler);
    if (err < 0) {
      err_str = komp_app_error("Failed to decode frame", &err_len);
      break;
    }

    if (komp_app.komp_im_in.im == VK_NULL_HANDLE) {
      komp_app.enc_vid_handler.width = komp_app.dec_vid_handler.width;
      komp_app.enc_vid_handler.height = komp_app.dec_vid_handler.height;
      komp_app.enc_vid_handler.sectioned_u = komp_app.dec_vid_handler.sectioned_u;
      komp_app.enc_vid_handler.sectioned_v = komp_app.dec_vid_handler.sectioned_v;

      komp_app.enc_vid_handler.framerate = komp_app.dec_vid_handler.framerate;
      komp_app.enc_vid_handler.time_base = komp_app.dec_vid_handler.time_base;

      komp_app.enc_vid_handler.pix_fmt = komp_app.dec_vid_handler.pix_fmt;
      /* komp_app.enc_vid_handler.color_range = komp_app.dec_vid_handler.cdc_ctx->color_range;
      komp_app.enc_vid_handler.color_primaries = komp_app.dec_vid_handler.cdc_ctx->color_primaries;
      komp_app.enc_vid_handler.color_trc = komp_app.dec_vid_handler.cdc_ctx->color_trc;
      komp_app.enc_vid_handler.colorspace = komp_app.dec_vid_handler.cdc_ctx->colorspace;
      komp_app.enc_vid_handler.field_order = komp_app.dec_vid_handler.cdc_ctx->field_order;
      komp_app.enc_vid_handler.chroma_sample_location = komp_app.dec_vid_handler.cdc_ctx->chroma_sample_location; */

      komp_app.enc_vid_handler.speed = speed;
      komp_app.enc_vid_handler.bitrate = av_rescale(1, bitrate.num, bitrate.den);

      err = vid_handler_set_encoder_params(&komp_app.enc_vid_handler, argv[o + 1]);
      if (err < 0) {
        err_str = komp_app_error("Failed to setup output video handler", &err_len);
        break;
      }

      if (komp_app.dec_vid_handler.frame->data[3] != NULL) {
        komp_app.komp_mem.buf_sizes[0] = 2 * komp_app.dec_vid_handler.plane + 2 * komp_app.dec_vid_handler.semiplane;
      } else {
        komp_app.komp_mem.buf_sizes[0] = komp_app.dec_vid_handler.plane + 2 * komp_app.dec_vid_handler.semiplane;
      }
      komp_app.komp_mem.buf_sizes[1] = 4 * komp_app.dec_vid_handler.plane;
      komp_app.komp_mem.buf_offsets[1] = komp_app.komp_mem.buf_sizes[0];
      komp_app.komp_mem.mem_size = komp_app.komp_mem.buf_sizes[0] + komp_app.komp_mem.buf_sizes[1];

      err = komp_mem_instanciate(&komp_app.komp_mem, komp_app.phys_dev, komp_app.dev);
      if (err != VK_SUCCESS) {
        err_str = komp_app_error("Failed to instantiate memory in the device", &err_len);
        break;
      }

      komp_app.komp_mem.buf_infos[0] = (VkBufferCreateInfo){
        VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
        NULL,
        NO_FLAGS,
        komp_app.komp_mem.buf_sizes[0],
        VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
        VK_SHARING_MODE_EXCLUSIVE,
        1,
        &komp_app.index.compute_family,
      };
      komp_app.komp_mem.buf_infos[1] = (VkBufferCreateInfo){
        VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
        NULL,
        NO_FLAGS,
        komp_app.komp_mem.buf_sizes[1],
        VK_BUFFER_USAGE_TRANSFER_DST_BIT,
        VK_SHARING_MODE_EXCLUSIVE,
        1,
        &komp_app.index.compute_family,
      };
      komp_app.komp_mem.buf_cnt = 2;
      err = komp_mem_bind_bufs(&komp_app.komp_mem, komp_app.dev);
      if (err != VK_SUCCESS) {
        err_str = komp_app_error("Failed to bind memory buffers", &err_len);
        break;
      }

      komp_app.komp_im_in.width = komp_app.dec_vid_handler.width;
      komp_app.komp_im_in.height = komp_app.dec_vid_handler.height;
      komp_app.komp_im_in.sectioned_u = komp_app.dec_vid_handler.sectioned_u;
      komp_app.komp_im_in.sectioned_v = komp_app.dec_vid_handler.sectioned_v;
      komp_app.komp_im_in.plane = komp_app.dec_vid_handler.plane;
      komp_app.komp_im_in.semiplane = komp_app.dec_vid_handler.semiplane;
      komp_app.komp_im_out.width = komp_app.enc_vid_handler.width;
      komp_app.komp_im_out.height = komp_app.enc_vid_handler.height;
      komp_app.komp_im_out.sectioned_u = komp_app.enc_vid_handler.sectioned_u;
      komp_app.komp_im_out.sectioned_v = komp_app.enc_vid_handler.sectioned_v;

      param = (PushParam){
        komp_app.komp_im_in.width,
        komp_app.komp_im_in.height,
        1.0f / (float)komp_app.komp_im_in.width,
        1.0f / (float)komp_app.komp_im_in.height,
      };

      komp_app.komp_im_in.im_info = (VkImageCreateInfo){
        VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
        NULL,
        NO_FLAGS,
        VK_IMAGE_TYPE_2D,
        komp_app.dec_vid_handler.fmt,
        (VkExtent3D){
          komp_app.dec_vid_handler.width,
          komp_app.dec_vid_handler.height,
          1,
        },
        1,
        1,
        VK_SAMPLE_COUNT_1_BIT,
        VK_IMAGE_TILING_OPTIMAL,
        VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
        VK_SHARING_MODE_EXCLUSIVE,
        0,
        NULL,
        VK_IMAGE_LAYOUT_UNDEFINED,
      };
      komp_app.komp_im_in.fmt = komp_app.komp_im_in.im_info.format;
      komp_app.komp_im_in.im_view = VK_NULL_HANDLE;
      komp_app.komp_im_in.sampler = VK_NULL_HANDLE;
      komp_app.komp_im_in.ycbcr_conv = VK_NULL_HANDLE;
      err = vkCreateImage(komp_app.dev, &komp_app.komp_im_in.im_info, NULL, &komp_app.komp_im_in.im);
      if (err != VK_SUCCESS) {
        err_str = komp_app_error("Failed to create input image", &err_len);
        ret = 1;
        break;
      }
      komp_app.komp_im_in.im_off = 0;
      komp_app.komp_im_in.im_mem = VK_NULL_HANDLE;
      err = komp_im_alloc(&komp_app.komp_im_in, &komp_app.komp_mem, komp_app.dev);
      if (err != VK_SUCCESS) {
        err_str = komp_app_error("Failed to allocate input image", &err_len);
        break;
      }
      komp_app.komp_im_out.im_info = (VkImageCreateInfo){
        VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
        NULL,
        NO_FLAGS,
        VK_IMAGE_TYPE_2D,
        VK_FORMAT_R8G8B8A8_UNORM,
        (VkExtent3D){
          komp_app.dec_vid_handler.width,
          komp_app.dec_vid_handler.height,
          1,
        },
        1,
        1,
        VK_SAMPLE_COUNT_1_BIT,
        VK_IMAGE_TILING_OPTIMAL,
        VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_STORAGE_BIT,
        VK_SHARING_MODE_EXCLUSIVE,
        0,
        NULL,
        VK_IMAGE_LAYOUT_UNDEFINED,
      };
      komp_app.komp_im_out.fmt = komp_app.komp_im_out.im_info.format;
      komp_app.komp_im_out.im_view = VK_NULL_HANDLE;
      komp_app.komp_im_out.sampler = VK_NULL_HANDLE;
      komp_app.komp_im_out.ycbcr_conv = VK_NULL_HANDLE;
      err = vkCreateImage(komp_app.dev, &komp_app.komp_im_out.im_info, NULL, &komp_app.komp_im_out.im);
      if (err != VK_SUCCESS) {
        err_str = komp_app_error("Failed to create output image", &err_len);
        ret = 1;
        break;
      }
      komp_app.komp_im_out.im_off = 0;
      komp_app.komp_im_out.im_mem = VK_NULL_HANDLE;
      err = komp_im_alloc(&komp_app.komp_im_out, &komp_app.komp_mem, komp_app.dev);
      if (err != VK_SUCCESS) {
        err_str = komp_app_error("Failed to allocate output image", &err_len);
        break;
      }

      err = komp_im_create_im_view_ycbcr(&komp_app.komp_im_in, komp_app.dev);
      if (err != VK_SUCCESS) {
        err_str = komp_app_error("Failed to create YUV image view", &err_len);
        break;
      }

      err = komp_im_create_sampler_ycbcr(&komp_app.komp_im_in, komp_app.dev);
      if (err != VK_SUCCESS) {
        err_str = komp_app_error("Failed to create YUV image sampler", &err_len);
        break;
      }

      err = komp_im_create_im_view_rgba8(&komp_app.komp_im_out, komp_app.dev);
      if (err != VK_SUCCESS) {
        err_str = komp_app_error("Failed to create RGBA image view", &err_len);
        break;
      }

      komp_app.komp_pipe.push_const = (VkPushConstantRange){ VK_SHADER_STAGE_COMPUTE_BIT, 0, sizeof(PushParam) };
      err = komp_pipe_set_desc_set_layout(&komp_app.komp_pipe, komp_app.dev, &komp_app.komp_im_in);
      if (err != VK_SUCCESS) {
        err_str = komp_app_error("Failed to set descriptor layout in pipeline", &err_len);
        break;
      }
      komp_app.komp_pipe.pipe_layout_info = (VkPipelineLayoutCreateInfo){
        VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
        NULL,
        NO_FLAGS,
        1,
        &komp_app.komp_pipe.desc_set_layout,
        1,
        &komp_app.komp_pipe.push_const,
      };
      err = vkCreatePipelineLayout(komp_app.dev, &komp_app.komp_pipe.pipe_layout_info, NULL, &komp_app.komp_pipe.pipe_layout);
      if (err != VK_SUCCESS) {
        err_str = komp_app_error("Pipeline layout couldn't be created", &err_len);
        break;
      }
      err = komp_pipe_create_compute_pipe(&komp_app.komp_pipe, komp_app.dev, komp_app.compute_shader);
      if (err != VK_SUCCESS) {
        err_str = komp_app_error("Compute pipeline couldn't be created", &err_len);
        break;
      }

      desc_set_alloc_info = (VkDescriptorSetAllocateInfo){
        VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO, NULL, komp_app.desc_pool, 1, &komp_app.komp_pipe.desc_set_layout,
      };
      err = vkAllocateDescriptorSets(komp_app.dev, &desc_set_alloc_info, &komp_app.desc_set);
      if (err != VK_SUCCESS) {
        err_str = komp_app_error("Failed to allocate descriptor sets", &err_len);
        break;
      }

      desc_buf_info_im_in = (VkDescriptorBufferInfo){
        komp_app.komp_mem.bufs[0],
        0,
        komp_app.komp_mem.buf_sizes[0],
      };
      desc_buf_info_im_out = (VkDescriptorBufferInfo){
        komp_app.komp_mem.bufs[1],
        0,
        komp_app.komp_mem.buf_sizes[1],
      };
      desc_buf_im_in = (VkDescriptorImageInfo){
        komp_app.komp_im_in.sampler,
        komp_app.komp_im_in.im_view,
        VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
      };
      desc_buf_im_out = (VkDescriptorImageInfo){
        VK_NULL_HANDLE,
        komp_app.komp_im_out.im_view,
        VK_IMAGE_LAYOUT_GENERAL,
      };
      write_desc_set = (VkWriteDescriptorSet[2]){
        (VkWriteDescriptorSet){
          VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
          NULL,
          komp_app.desc_set,
          0,
          0,
          1,
          VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
          &desc_buf_im_in,
          &desc_buf_info_im_in,
          NULL,
        },
        (VkWriteDescriptorSet){
          VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
          NULL,
          komp_app.desc_set,
          1,
          0,
          1,
          VK_DESCRIPTOR_TYPE_STORAGE_IMAGE,
          &desc_buf_im_out,
          &desc_buf_info_im_out,
          NULL,
        },
      };
      vkUpdateDescriptorSets(komp_app.dev, 2, write_desc_set, 0, NULL);

      err = komp_im_transition_im(
        &komp_app.komp_im_in, NULL, komp_app.dev, komp_app.cmd_pool, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        komp_app.index);
      if (err != VK_SUCCESS) {
        err_str = komp_app_error("Failed to transition image", &err_len);
        break;
      }

      err = komp_im_transition_im(
        &komp_app.komp_im_out, NULL, komp_app.dev, komp_app.cmd_pool, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_GENERAL, komp_app.index);
      if (err != VK_SUCCESS) {
        err_str = komp_app_error("Failed to transition image", &err_len);
        break;
      }
    }

    err = vid_handler_submit_frame(
      &komp_app.dec_vid_handler, komp_app.dev, komp_app.komp_mem.dev_mem, komp_app.komp_mem.buf_offsets[0], komp_app.komp_mem.buf_sizes[0]);
    if (err != VK_SUCCESS) {
      err_str = komp_app_error("Failed to upload image", &err_len);
      break;
    }

    err = komp_im_copy_buf_to_yuv_im(&komp_app.komp_im_in, komp_app.komp_mem.bufs[0], komp_app.dev, komp_app.cmd_pool, komp_app.index);
    if (err != VK_SUCCESS) {
      err_str = komp_app_error("Failed to copy buffer to YUV image", &err_len);
      break;
    }

    err = komp_im_transition_im(
      &komp_app.komp_im_in, NULL, komp_app.dev, komp_app.cmd_pool, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
      VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, komp_app.index);
    if (err != VK_SUCCESS) {
      err_str = komp_app_error("Failed to transition image", &err_len);
      break;
    }

    err = komp_cmd_begin(&komp_app.cmd_run, komp_app.dev, komp_app.cmd_pool);
    if (err != VK_SUCCESS) {
      err_str = komp_app_error("Failed to begin recording command buffer", &err_len);
      break;
    }
    vkCmdBindPipeline(komp_app.cmd_run, VK_PIPELINE_BIND_POINT_COMPUTE, komp_app.komp_pipe.comp_pipe);
    vkCmdBindDescriptorSets(
      komp_app.cmd_run, VK_PIPELINE_BIND_POINT_COMPUTE, komp_app.komp_pipe.pipe_layout, 0, 1, &komp_app.desc_set, 0, NULL);
    vkCmdPushConstants(komp_app.cmd_run, komp_app.komp_pipe.pipe_layout, VK_SHADER_STAGE_COMPUTE_BIT, 0, sizeof(PushParam), &param);
    vkCmdDispatch(komp_app.cmd_run, komp_app.komp_im_out.width, komp_app.komp_im_out.height, 1);

    err = vkEndCommandBuffer(komp_app.cmd_run);
    if (err != VK_SUCCESS) {
      err_str = komp_app_error("Failed to end recording command buffer", &err_len);
      break;
    }

    vkGetDeviceQueue(komp_app.dev, komp_app.index.compute_family, 0, &compute_queue);
    compute_submit_info = (VkSubmitInfo){
      VK_STRUCTURE_TYPE_SUBMIT_INFO, NULL, 0, NULL, NULL, 1, &komp_app.cmd_run, 0, NULL,
    };
    err = vkQueueSubmit(compute_queue, 1, &compute_submit_info, VK_NULL_HANDLE);
    if (err != VK_SUCCESS) {
      err_str = komp_app_error("Failed to submit queue", &err_len);
      break;
    }

    err = vkQueueWaitIdle(compute_queue);
    if (err < 0) {
      err_str = komp_app_error("Failed to wait queue", &err_len);
      break;
    }

    vkFreeCommandBuffers(komp_app.dev, komp_app.cmd_pool, 1, &komp_app.cmd_run);

    err = komp_im_transition_im(
      &komp_app.komp_im_out, &komp_app.komp_mem, komp_app.dev, komp_app.cmd_pool, VK_IMAGE_LAYOUT_GENERAL,
      VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, komp_app.index);
    if (err != VK_SUCCESS) {
      err_str = komp_app_error("Failed to transition image", &err_len);
      break;
    }

    err = komp_im_copy_rgb_im_to_buf(&komp_app.komp_im_out, komp_app.komp_mem.bufs[1], komp_app.dev, komp_app.cmd_pool, komp_app.index);
    if (err != VK_SUCCESS) {
      err_str = komp_app_error("Failed to copy RGBA image to buffer", &err_len);
      break;
    }

    err = vid_handler_receive_frame(
      &komp_app.enc_vid_handler, komp_app.dev, komp_app.komp_mem.dev_mem, komp_app.komp_mem.buf_offsets[1], komp_app.komp_mem.buf_sizes[1]);
    if (err != VK_SUCCESS) {
      err_str = komp_app_error("Failed to download image", &err_len);
      break;
    }

    err = vid_handler_encode_frame(&komp_app.enc_vid_handler);
    if (err < 0) {
      err_str = komp_app_error("Failed to submit frame to encoder", &err_len);
      break;
    }

    err = komp_im_transition_im(
      &komp_app.komp_im_in, NULL, komp_app.dev, komp_app.cmd_pool, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
      VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, komp_app.index);
    if (err != VK_SUCCESS) {
      err_str = komp_app_error("Failed to transition image", &err_len);
      break;
    }

    err = komp_im_transition_im(
      &komp_app.komp_im_out, NULL, komp_app.dev, komp_app.cmd_pool, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, VK_IMAGE_LAYOUT_GENERAL,
      komp_app.index);
    if (err != VK_SUCCESS) {
      err_str = komp_app_error("Failed to transition image", &err_len);
      break;
    }
  }

  if (err != VK_SUCCESS) {
    if (err_str != NULL) {
      komp_print_error(err_str, err_len, err);
      free(err_str);
    }
    ret = 1;
  }

  if (komp_app.enc_vid_handler.fmt_ctx != NULL) {
    komp_app.enc_vid_handler.eof = true;
    if (komp_app.enc_vid_handler.cdc_ctx != NULL) {
      vid_handler_encode_frame(&komp_app.enc_vid_handler);
    }
    av_write_trailer(komp_app.enc_vid_handler.fmt_ctx);
  }

  err = vkDeviceWaitIdle(komp_app.dev);
  if (err != VK_SUCCESS) {
    err_str = komp_app_error("Failed to wait device", &err_len);
    ret = 1;
  }

  komp_app_destroy(&komp_app);

  return ret;
}

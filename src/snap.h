#ifndef SNAP_H
#define SNAP_H



#include <stdbool.h>
#include <stdint.h>



/**
 * encode_heif_image_ycbcr:
 * @y_bin: Luminance data.
 * @cb_bin: Red chrominance data.
 * @cr_bin: Blue chrominance data.
 * @width: Width of the image.
 * @height: Height of the image.
 * @filename: Path to an output AVIF image.
 *
 * Encodes the supplied planar YUV data (@y_bin, @cb_bin, @cr_bin) and outputs an AVIF image at @filename path.
 */
bool encode_heif_image_ycbcr(
  const uint8_t **y_bin, const uint8_t **cb_bin, const uint8_t **cr_bin, uint32_t width, uint32_t height, bool strided,
  const char *filename);

/**
 * encode_heif_image_ycbcr:
 * @bin: RGBA data.
 * @width: Width of the image.
 * @height: Height of the image.
 * @filename: Path to an output AVIF image.
 *
 * Encodes the supplied RGBA data (@bin) and outputs an AVIF image at @filename path.
 */
bool encode_heif_image_rgba(const uint8_t **bin, uint32_t width, uint32_t height, const char *filename);



#endif /* SNAP_H */

#ifndef KOMP_TEXTURE_H
#define KOMP_TEXTURE_H



#include <vulkan/vulkan.h>

#include "common.h"
#include "device.h"



/**
 * PushParam:
 * @width: Texture width in the shader.
 * @height: Texture height in the shader.
 * @frame_width: Normalized width of the texture in the shader.
 * @frame_height: Normalized height of the texture in the shader.
 *
 * Push parameters representing the texture dimensions for a compute shader.
 */
typedef struct _PushParam {
  uint32_t width;
  uint32_t height;
  float frame_width;
  float frame_height;
} PushParam;

/**
 * KomputeImage:
 * @im: Image handle.
 * @im_view: Image view handle.
 * @sampler: Image sampler handle.
 * @im_mem: Image memory handle.
 * @ycbcr_conv: YCbCr conversion handle.
 * @im_info: Image creation info.
 * @im_view_info: Image view creation info.
 * @ycbcr_conv_info: YCbCr conversion creation info, used with YUV formats.
 * @ycbcr_sampler_info: YCbCr sampler creation info, used with YUV formats.
 * @im_size: Image size in bytes.
 * @im_off: Image offset in memory in bytes.
 * @fmt: Image format, planar YUV or RGB (alpha optionally) for input, RGBA for output.
 * @width: Texture width in the shader.
 * @height: Texture height in the shader.
 * @sectioned_u: Width of the chromacity planes, unused with RGB formats.
 * @sectioned_v: Height of the chromacity planes, unused with RGB formats.
 * @plane: Computed size in bytes of the luminance or the RGB plane.
 * @semiplane: Computed size in bytes of the chromacity planes, unused with RGB formats.
 *
 * Texture abstraction type for texture management. It supports both planar YUV and RGB. Alpha channels are only used with RGB.
 */
typedef struct _KomputeImage {
  VkImage im;
  VkImageView im_view;
  VkSampler sampler;
  VkDeviceMemory im_mem;
  VkSamplerYcbcrConversion ycbcr_conv;

  VkImageCreateInfo im_info;
  VkImageViewCreateInfo im_view_info;
  VkSamplerYcbcrConversionCreateInfo ycbcr_conv_info;
  VkSamplerYcbcrConversionInfo ycbcr_sampler_info;

  VkDeviceSize im_size;
  VkDeviceSize im_off;
  VkFormat fmt;
  int width;
  int height;
  int sectioned_u;
  int sectioned_v;

  uint32_t plane;
  uint32_t semiplane;
} KomputeImage;



/**
 * komp_im_alloc:
 * @komp_im: Pointer to a texture manager structure.
 * @komp_mem: Pointer to a memory manager structure.
 * @dev: A logical device handle.
 *
 * Allocates an image with parameters passed through the @komp_im texture manager.
 *
 *
 * Returns: On success `VK_SUCCESS`, otherwise another apropiate `VkResult` is returned.
 */
VkResult komp_im_alloc(KomputeImage *komp_im, KomputeMem *komp_mem, VkDevice dev);

/**
 * komp_im_transition_im:
 * @komp_im: Pointer to a texture manager structure.
 * @komp_mem: Pointer to a memory manager structure.
 * @dev: A logical device handle.
 * @cmd_pool: A command pool handle.
 * @src_layout: Old layout of the image.
 * @dst_layout: New layout of the image.
 * @queue_index: Index to the compute queue family.
 *
 * Transitions the image of the @komp_im texture manager from the old layout @src_layout to the new layout @dst_layout.
 *
 *
 * Returns: On success `VK_SUCCESS`, otherwise another apropiate `VkResult` is returned.
 */
VkResult komp_im_transition_im(
  KomputeImage *komp_im, KomputeMem *komp_mem, VkDevice dev, VkCommandPool cmd_pool, VkImageLayout src_layout, VkImageLayout dst_layout,
  QueueIndex queue_index);

/**
 * komp_im_copy_buf_to_yuv_im:
 * @komp_im: Pointer to a texture manager structure.
 * @src_buf: A memory buffer handle, used as the copy source.
 * @dev: A logical device handle.
 * @cmd_pool: A command pool handle.
 * @queue_index: Index to the compute queue family.
 *
 * Copies the @src_buf source buffer to a YUV image in the @komp_im texture manager.
 *
 *
 * Returns: On success `VK_SUCCESS`, otherwise another apropiate `VkResult` is returned.
 */
VkResult komp_im_copy_buf_to_yuv_im(KomputeImage *komp_im, VkBuffer src_buf, VkDevice dev, VkCommandPool cmd_pool, QueueIndex queue_index);

/**
 * komp_im_copy_rgb_im_to_buf:
 * @komp_im: Pointer to a texture manager structure.
 * @dst_buf: A memory buffer handle, used as the copy destination.
 * @dev: A logical device handle.
 * @cmd_pool: A command pool handle.
 * @queue_index: Index to the compute queue family.
 *
 * Copies an RGBA image of the @komp_im texture manager to the @dst_buf destination buffer.
 *
 *
 * Returns: On success `VK_SUCCESS`, otherwise another apropiate `VkResult` is returned.
 */
VkResult komp_im_copy_rgb_im_to_buf(KomputeImage *komp_im, VkBuffer dst_buf, VkDevice dev, VkCommandPool cmd_pool, QueueIndex queue_index);

/**
 * komp_im_create_im_view_ycbcr:
 * @komp_im: Pointer to a texture manager structure.
 * @dev: A logical device handle.
 *
 * Creates a YUV image view in the @komp_im texture manager.
 *
 *
 * Returns: On success `VK_SUCCESS`, otherwise another apropiate `VkResult` is returned.
 */
VkResult komp_im_create_im_view_ycbcr(KomputeImage *komp_im, VkDevice dev);

/**
 * komp_im_create_im_view_rgba8:
 * @komp_im: Pointer to a texture manager structure.
 * @dev: A logical device handle.
 *
 * Creates an RGBA image view in the @komp_im texture manager.
 *
 *
 * Returns: On success `VK_SUCCESS`, otherwise another apropiate `VkResult` is returned.
 */
VkResult komp_im_create_im_view_rgba8(KomputeImage *komp_im, VkDevice dev);

/**
 * komp_im_create_sampler_ycbcr:
 * @komp_im: Pointer to a texture manager structure.
 * @dev: A logical device handle.
 *
 * Creates a YUV image sampler in the @komp_im texture manager.
 *
 *
 * Returns: On success `VK_SUCCESS`, otherwise another apropiate `VkResult` is returned.
 */
VkResult komp_im_create_sampler_ycbcr(KomputeImage *komp_im, VkDevice dev);

/**
 * komp_im_destroy:
 * @komp_im: Pointer to a texture manager structure.
 * @dev: A logical device handle.
 *
 * Destroys a @komp_im texture manager.
 */
void komp_im_destroy(KomputeImage *komp_im, VkDevice dev);



#endif /* KOMP_TEXTURE_H */

#ifndef COMMON_H
#define COMMON_H



/**
 * SECTION: common
 * @title: Common
 * @short_description: Common utilities VulkanVideoCompute.
 *
 * Common utilities for VulkanVideoCompute.
 */


#define NO_FLAGS 0
#define EMPTY_STRUCT 0

#define MACH_SIZE (8 * sizeof(size_t))
#define SIZE_MASK (MACH_SIZE - 1)
#define ALIGN_SIZE (8 * sizeof(uint16_t))
#define ALIGN_MASK (ALIGN_SIZE - 1)

#define MIN(X, Y) (((X) < (Y)) ? (X) : (Y))
#define MAX(X, Y) (((X) > (Y)) ? (X) : (Y))

/**
 * YOption:
 * @SOME: Value is existent.
 * @NONE: Value is non-existent.
 */
typedef enum _YOption {
  SOME,
  NONE,
} YOption;



#endif /* COMMON_H */

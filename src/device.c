#include <vulkan/vulkan.h>

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include "device.h"



/**
 * SECTION: device
 * @title: Device
 * @short_description: Device management for VulkanVideoCompute.
 *
 * This module provides basic Vulkan device and memory management conveniences for VulkanVideoCompute.
 */



bool check_physical_device(VkPhysicalDevice dev, VkPhysicalDeviceFeatures *dev_features) {
  VkPhysicalDeviceProperties dev_props;

  vkGetPhysicalDeviceProperties(dev, &dev_props);
  vkGetPhysicalDeviceFeatures(dev, dev_features);

  return dev_props.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU;
}

VkPhysicalDevice retrieve_physical_device(VkInstance instance, VkPhysicalDeviceFeatures *dev_features) {
  VkPhysicalDevice *devs;
  VkPhysicalDevice phys_dev;
  uint32_t dev_cnt;

  phys_dev = VK_NULL_HANDLE;
  dev_cnt = 0;
  vkEnumeratePhysicalDevices(instance, &dev_cnt, NULL);
  devs = (VkPhysicalDevice *)malloc(sizeof(VkPhysicalDevice) * dev_cnt);
  vkEnumeratePhysicalDevices(instance, &dev_cnt, devs);
  if (dev_cnt == 0) {
    free(devs);
    return phys_dev;
  }
  for (int i = 0; i < dev_cnt; i++) {
    VkPhysicalDevice dev;

    dev = devs[i];
    if (check_physical_device(dev, dev_features)) {
      phys_dev = dev;
      break;
    }
  }

  free(devs);

  return phys_dev;
}

QueueIndex discover_queue_families(VkPhysicalDevice dev) {
  QueueIndex index;
  VkQueueFamilyProperties *fam_props;
  uint32_t fam_prop_cnt;

  index = (QueueIndex){ NONE, 0 };
  vkGetPhysicalDeviceQueueFamilyProperties(dev, &fam_prop_cnt, NULL);
  fam_props = (VkQueueFamilyProperties *)malloc(sizeof(VkQueueFamilyProperties) * fam_prop_cnt);
  vkGetPhysicalDeviceQueueFamilyProperties(dev, &fam_prop_cnt, fam_props);
  for (int i = 0; i < fam_prop_cnt; i++) {
    VkQueueFamilyProperties fam_prop;

    fam_prop = fam_props[i];
    if (fam_prop.queueFlags & VK_QUEUE_COMPUTE_BIT) {
      index.compute_family = i;
      index.compute_option = SOME;
      break;
    }
  }

  free(fam_props);

  return index;
}

VkDevice create_device(VkPhysicalDevice phys_dev, VkPhysicalDeviceFeatures *phys_dev_features, QueueIndex index) {
  VkPhysicalDeviceVulkan11Features phys_dev_vk_1dot1_features;
  float priority;
  VkDeviceQueueCreateInfo dev_queue_info;
  VkDeviceCreateInfo dev_info;
  VkDevice dev;

  phys_dev_vk_1dot1_features = (VkPhysicalDeviceVulkan11Features){
    VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_1_FEATURES,
  };
  phys_dev_vk_1dot1_features.samplerYcbcrConversion = VK_TRUE;
  priority = 1.0f;
  dev_queue_info = (VkDeviceQueueCreateInfo){
    VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO, NULL, NO_FLAGS, index.compute_family, 1, &priority,
  };
  dev_info = (VkDeviceCreateInfo){
    VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO, &phys_dev_vk_1dot1_features, NO_FLAGS, 1, &dev_queue_info, 0, NULL, 0, NULL, phys_dev_features,
  };
  if (vkCreateDevice(phys_dev, &dev_info, NULL, &dev) != VK_SUCCESS) {
    return VK_NULL_HANDLE;
  }

  return dev;
}

VkResult komp_mem_instanciate(KomputeMem *komp_mem, VkPhysicalDevice phys_dev, VkDevice dev) {
  komp_mem->mem_type_index = VK_MAX_MEMORY_TYPES;

  vkGetPhysicalDeviceMemoryProperties(phys_dev, &komp_mem->mem_props);

  for (uint32_t i = 0; i < komp_mem->mem_props.memoryTypeCount; i++) {
    VkMemoryPropertyFlags flag_set;
    VkMemoryPropertyFlagBits flag_prop_set;

    flag_set = komp_mem->mem_props.memoryTypes[i].propertyFlags;
    flag_prop_set = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;
    if (
      ((flag_set & flag_prop_set) == flag_prop_set) &&
      (komp_mem->mem_size < komp_mem->mem_props.memoryHeaps[komp_mem->mem_props.memoryTypes[i].heapIndex].size)) {
      komp_mem->mem_type_index = i;
      break;
    }
  }
  if (komp_mem->mem_type_index == VK_MAX_MEMORY_TYPES) {
    return VK_ERROR_OUT_OF_DEVICE_MEMORY;
  }
  komp_mem->mem_alloc_info = (VkMemoryAllocateInfo){
    VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
    NULL,
    komp_mem->mem_size,
    komp_mem->mem_type_index,
  };

  return vkAllocateMemory(dev, &komp_mem->mem_alloc_info, NULL, &komp_mem->dev_mem);
}

VkResult komp_mem_bind_bufs(KomputeMem *komp_mem, VkDevice dev) {
  VkResult ret;

  for (size_t b = 0; b < komp_mem->buf_cnt; b++) {
    ret = vkCreateBuffer(dev, &komp_mem->buf_infos[b], NULL, &komp_mem->bufs[b]);
    if (ret != VK_SUCCESS) {
      komp_mem->buf_cnt = b;
      return ret;
    }

    ret = vkBindBufferMemory(dev, komp_mem->bufs[b], komp_mem->dev_mem, komp_mem->buf_offsets[b]);
    if (ret != VK_SUCCESS) {
      komp_mem->buf_cnt = b;
      vkDestroyBuffer(dev, komp_mem->bufs[b], NULL);
      return ret;
    }
  }

  return ret;
}

void komp_mem_destroy(KomputeMem *komp_mem, VkDevice dev) {
  VkResult ret;

  for (size_t b = 0; b < komp_mem->buf_cnt; b++) {
    vkDestroyBuffer(dev, komp_mem->bufs[b], NULL);
    komp_mem->bufs[b] = VK_NULL_HANDLE;
  }
  if (komp_mem->dev_mem != VK_NULL_HANDLE) {
    vkFreeMemory(dev, komp_mem->dev_mem, NULL);
    komp_mem->dev_mem = VK_NULL_HANDLE;
  }
}

VkResult komp_cmd_begin(VkCommandBuffer *cmd, VkDevice dev, VkCommandPool cmd_pool) {
  VkResult ret;

  VkCommandBufferAllocateInfo cmd_buf_alloc_info;
  VkCommandBufferBeginInfo cmd_buf_begin_info;

  cmd_buf_alloc_info = (VkCommandBufferAllocateInfo){
    VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO, NULL, cmd_pool, VK_COMMAND_BUFFER_LEVEL_PRIMARY, 1,
  };

  ret = vkAllocateCommandBuffers(dev, &cmd_buf_alloc_info, cmd);
  if (ret != VK_SUCCESS) {
    return ret;
  }

  cmd_buf_begin_info = (VkCommandBufferBeginInfo){
    VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
    NULL,
    VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT,
    NULL,
  };

  ret = vkBeginCommandBuffer(*cmd, &cmd_buf_begin_info);
  if (ret != VK_SUCCESS) {
    vkFreeCommandBuffers(dev, cmd_pool, 1, cmd);
  }

  return ret;
}

#ifndef KOMP_PIPE_H
#define KOMP_PIPE_H



#include <vulkan/vulkan.h>

#include <stdbool.h>

#include "texture.h"



typedef struct komputepipeline {
  VkDescriptorSetLayoutBinding *desc_set_layout_bindings;
  VkSpecializationMapEntry *spec_map_entries;

  VkDescriptorSetLayout desc_set_layout;
  VkPipelineLayout pipe_layout;
  VkPipeline comp_pipe;

  VkPushConstantRange push_const;
  VkDescriptorSetLayoutCreateInfo desc_set_layout_info;
  VkPipelineLayoutCreateInfo pipe_layout_info;
  VkSpecializationInfo spec_info;
  VkComputePipelineCreateInfo comp_pipe_info;
} KomputePipeline;


/**
 * komp_pipe_set_desc_set_layout:
 * @pipe: Pointer to a compute pipeline handler.
 * @dev: VkDevice handle of a logical Vulkan device.
 *
 * Creates and sets a descriptor layout in the @pipe compute pipeline manager.
 *
 *
 * Returns: On success `VK_SUCCESS` or a Vulkan error on failure.
 */
VkResult komp_pipe_set_desc_set_layout(KomputePipeline *pipe, VkDevice dev, KomputeImage *im);

/**
 * komp_pipe_create_compute_pipe:
 * @pipe: Pointer to a compute pipeline handler.
 * @dev: VkDevice handle of a logical Vulkan device.
 * @shader: Compute shader module.
 *
 * Creates a new compute pipeline in the @pipe compute pipeline manager.
 *
 *
 * Returns: On success `VK_SUCCESS` or a Vulkan error on failure.
 */
VkResult komp_pipe_create_compute_pipe(KomputePipeline *pipe, VkDevice dev, VkShaderModule shader);

/**
 * komp_pipe_destroy:
 * @pipe: Pointer to a compute pipeline handler.
 * @vid_handler: Pointer to a video handler structure.
 * @dev: VkDevice handle of a logical Vulkan device.
 *
 * Destroys a @komp_pipe compute pipeline manager.
 */
void komp_pipe_destroy(KomputePipeline *pipe, VkDevice dev);

/**
 * create_shader_module:
 * @dev: VkDevice handle of a logical Vulkan device.
 * @shader: Pointer to a shader module.
 * @path: Path to the spirv binary.
 * @spirv: Pointer to the spirv binary.
 *
 * Creates a @shader shader module from the SPIRV binary pointed by @path filesystem path, @spirv is set to this binary.
 *
 *
 * Returns: On success `VK_SUCCESS` or a Vulkan error on failure.
 */
VkResult create_shader_module(VkDevice dev, VkShaderModule *shader, const char *path, const uint32_t **spirv);



#endif /* KOMP_PIPE_H */

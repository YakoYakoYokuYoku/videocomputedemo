#include <libheif/heif.h>

#include <stdbool.h>
#include <string.h>

#include "common.h"
#include "snap.h"



/**
 * SECTION: snap
 * @title: Snap
 * @short_description: Snapshots for VulkanVideoCompute.
 *
 * This module provides snapshot functionalities for debugging frame uploading, processing and downloading by VulkanVideoCompute.
 */



bool encode_heif_image_ycbcr(
  const uint8_t **y_bin, const uint8_t **cb_bin, const uint8_t **cr_bin, uint32_t width, uint32_t height, bool strided,
  const char *filename) {
  struct heif_context *heif_ctx;
  struct heif_encoder *heif_enc;
  struct heif_image *heif_im;
  struct heif_error heif_err;
  uint8_t *y;
  uint8_t *cb;
  uint8_t *cr;
  uint8_t *a;
  const uint8_t *y_data;
  const uint8_t *cb_data;
  const uint8_t *cr_data;
  const uint8_t *a_data;
  int y_stride;
  int cb_stride;
  int cr_stride;
  int a_stride;
  uint32_t linesize;
  uint32_t linesemisize;

  if (strided) {
    linesize = (width & ~SIZE_MASK) + MACH_SIZE;
    linesemisize = ((width / 2) & ~SIZE_MASK) + MACH_SIZE;
  }

  heif_ctx = heif_context_alloc();
  heif_err = heif_context_get_encoder_for_format(heif_ctx, heif_compression_AV1, &heif_enc);
  if (heif_err.code != heif_error_Ok) {
    heif_context_free(heif_ctx);
    return false;
  }
  heif_encoder_set_lossy_quality(heif_enc, 80);
  heif_encoder_set_parameter_string(heif_enc, "chroma", "420");
  heif_err = heif_image_create(width, height, heif_colorspace_YCbCr, heif_chroma_420, &heif_im);
  if (heif_err.code != heif_error_Ok) {
    heif_context_free(heif_ctx);
    return false;
  }

  heif_err = heif_image_add_plane(heif_im, heif_channel_Y, linesize, height, 8);
  if (heif_err.code != heif_error_Ok) {
    heif_image_release(heif_im);
    heif_encoder_release(heif_enc);
    heif_context_free(heif_ctx);
    return false;
  }
  heif_err = heif_image_add_plane(heif_im, heif_channel_Cb, linesemisize, height, 8);
  if (heif_err.code != heif_error_Ok) {
    heif_image_release(heif_im);
    heif_encoder_release(heif_enc);
    heif_context_free(heif_ctx);
    return false;
  }
  heif_err = heif_image_add_plane(heif_im, heif_channel_Cr, linesemisize, height, 8);
  if (heif_err.code != heif_error_Ok) {
    heif_image_release(heif_im);
    heif_encoder_release(heif_enc);
    heif_context_free(heif_ctx);
    return false;
  }

  y = heif_image_get_plane(heif_im, heif_channel_Y, &y_stride);
  cb = heif_image_get_plane(heif_im, heif_channel_Cb, &cb_stride);
  cr = heif_image_get_plane(heif_im, heif_channel_Cr, &cr_stride);
  y_data = *y_bin;
  cb_data = *cb_bin;
  cr_data = *cr_bin;
  for (int i = 0; i < height; i++) {
    memcpy(y, y_data, y_stride);
    y_data += y_stride;
    y += y_stride;
  }
  for (int i = 0; i < height / 2; i++) {
    memcpy(cb, cb_data, cb_stride);
    memcpy(cr, cr_data, cr_stride);
    cb_data += cb_stride;
    cr_data += cr_stride;
    cb += cb_stride;
    cr += cr_stride;
  }

  heif_err = heif_context_encode_image(heif_ctx, heif_im, heif_enc, NULL, NULL);
  if (heif_err.code != heif_error_Ok) {
    heif_image_release(heif_im);
    heif_encoder_release(heif_enc);
    heif_context_free(heif_ctx);
    return false;
  }
  heif_err = heif_context_write_to_file(heif_ctx, filename);
  if (heif_err.code != heif_error_Ok) {
    heif_image_release(heif_im);
    heif_encoder_release(heif_enc);
    heif_context_free(heif_ctx);
    return false;
  }


  heif_image_release(heif_im);
  heif_encoder_release(heif_enc);
  heif_context_free(heif_ctx);


  return true;
}

bool encode_heif_image_rgba(const uint8_t **bin, uint32_t width, uint32_t height, const char *filename) {
  struct heif_context *heif_ctx;
  struct heif_encoder *heif_enc;
  struct heif_image *heif_im;
  struct heif_error heif_err;
  const uint8_t *rgba_data;
  uint8_t *rgba;
  int rgba_stride;


  heif_ctx = heif_context_alloc();
  heif_err = heif_context_get_encoder_for_format(heif_ctx, heif_compression_AV1, &heif_enc);
  if (heif_err.code != heif_error_Ok) {
    heif_context_free(heif_ctx);
    return false;
  }
  heif_encoder_set_lossy_quality(heif_enc, 80);
  heif_encoder_set_parameter_string(heif_enc, "chroma", "420");
  heif_err = heif_image_create(width, height, heif_colorspace_RGB, heif_chroma_interleaved_RGBA, &heif_im);
  if (heif_err.code != heif_error_Ok) {
    heif_context_free(heif_ctx);
    return false;
  }

  heif_err = heif_image_add_plane(heif_im, heif_channel_interleaved, width, height, 8);
  if (heif_err.code != heif_error_Ok) {
    heif_image_release(heif_im);
    heif_encoder_release(heif_enc);
    heif_context_free(heif_ctx);
    return false;
  }

  rgba = heif_image_get_plane(heif_im, heif_channel_interleaved, &rgba_stride);
  rgba_data = *bin;
  for (int i = 0; i < height; i++) {
    memcpy(rgba, rgba_data, rgba_stride);
    rgba_data += rgba_stride;
    rgba += rgba_stride;
  }

  heif_err = heif_context_encode_image(heif_ctx, heif_im, heif_enc, NULL, NULL);
  if (heif_err.code != heif_error_Ok) {
    heif_image_release(heif_im);
    heif_encoder_release(heif_enc);
    heif_context_free(heif_ctx);
    return false;
  }
  heif_err = heif_context_write_to_file(heif_ctx, filename);
  if (heif_err.code != heif_error_Ok) {
    heif_image_release(heif_im);
    heif_encoder_release(heif_enc);
    heif_context_free(heif_ctx);
    return false;
  }


  heif_image_release(heif_im);
  heif_encoder_release(heif_enc);
  heif_context_free(heif_ctx);


  return true;
}

#ifndef KOMP_VIDEO_H
#define KOMP_VIDEO_H



#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavutil/avutil.h>
#include <libavutil/pixfmt.h>
#include <libswscale/swscale.h>
#include <vulkan/vulkan.h>

#include <stdbool.h>



/**
 * KomputeVideoHandler:
 * @fmt_ctx: Format context for demuxing/muxing.
 * @cdc_ctx: Codec context for decoding/encoding.
 * @streams: Stream read-only parameters shared across an input and output video handler.
 * @sws_ctx: Software scaling/color conversion context.
 * @frame: Currently used frame.
 * @pkt: Currently used AV packet.
 * @stream_cnt: Stream count.
 * @video_idx: Video stream index.
 * @eof: End of stream check.
 * @again: Insufficient input check.
 * @speed: Encoder speed set from 0 to 10.
 * @pts: Current presentation timestamp.
 * @bitrate: Bitrate of the video.
 * @width: Width of the picture or the luminance plane.
 * @height: Height of the picture or the luminance plane.
 * @sectioned_u: Width of the chromacity planes.
 * @sectioned_v: Height of the chromacity planes.
 * @plane: Computed size in bytes of the luminance plane.
 * @semiplane: Computed size in bytes of the chromacity planes.
 * @fmt: Vulkan image format of the frame.
 * @pix_fmt: Pixel format of the codec.
 * @framerate: Framerate of the video.
 * @time_base: Time base of the video.
 *
 * Video handler structure for video processing with properties like video container, codec, dimensions, pixel format,
 * color, framerate and extra components for decoding/encoding, demuxing/muxing, I/O and color conversion.
 */
struct _KomputeVideoHandler {
  AVFormatContext *fmt_ctx;
  AVCodecContext *cdc_ctx;
  AVStream **streams;
  struct SwsContext *sws_ctx;
  AVFrame *frame;
  AVPacket *pkt;

  int32_t *stream_map;

  bool eof;
  bool again;

  uint8_t speed;

  uint8_t stream_cnt;
  int32_t video_idx;

  int64_t pts;
  int64_t bitrate;

  int32_t width;
  int32_t height;
  int32_t sectioned_u;
  int32_t sectioned_v;

  uint32_t plane;
  uint32_t semiplane;

  VkFormat fmt;
  enum AVPixelFormat pix_fmt;

  AVRational framerate;
  AVRational time_base;
};

typedef struct _KomputeVideoHandler KomputeVideoHandler;

/**
 * FmtSpec:
 * @fmt: Vulkan image format of the frame.
 * @half_width: Half horizontal resolution.
 * @half_height: Half vertical resolution.
 * @plane_count: Number of planes of the image.
 *
 * Structure that represents a Vulkan format and its plane characteristics.
 */
typedef struct _FmtSpec {
  VkFormat fmt;
  bool half_width;
  bool half_height;
  char plane_count;
} FmtSpec;



/**
 * vid_handler_obtain_fmt:
 * @fmt: An AVPixelFormat to map with an VkFormat.
 *
 * Tries to obtain the best VkFormat out of a AVPixelFormat.
 *
 *
 * Returns: A VkFormat mapped from the AVPixelFormat.
 */
FmtSpec vid_handler_obtain_fmt(enum AVPixelFormat fmt);

/**
 * vid_handler_open_dec_video:
 * @vid_handler: Pointer to a video handler structure.
 * @path: Location or URI to an input video.
 *
 * Opens an input video at the specified @path for decoding.
 *
 *
 * Returns: On success `0` or a positive integer and negative representing an `AVERROR` on failure.
 */
int vid_handler_open_dec_video(KomputeVideoHandler *vid_handler, const char *path);

/**
 * vid_handler_open_enc_video:
 * @vid_handler: Pointer to a video handler structure.
 * @path: Location or URI to an output video.
 *
 * Opens an output video at the specified @path for encoding.
 *
 *
 * Returns: On success `0` or a positive integer and negative representing an `AVERROR` on failure.
 */
int vid_handler_open_enc_video(KomputeVideoHandler *vid_handler, const char *path);

/**
 * vid_handler_open_enc_video:
 * @vid_handler: Pointer to a video handler structure.
 * @path: Location or URI to an output video.
 *
 * Sets up an @vid_handler output at the specified @path from #vid_handler_open_enc_video.
 *
 *
 * Returns: On success `0` or a positive integer and negative representing an `AVERROR` on failure.
 */
int vid_handler_set_encoder_params(KomputeVideoHandler *vid_handler, const char *path);

/**
 * vid_handler_copy_frame:
 * @src_vid_handler: Pointer to a source video handler structure.
 * @dst_vid_handler: Pointer to a destination video handler structure.
 *
 * Copies an encoded packet hold by the @src_vid_handler input source handler to the @dst_vid_handler destination video handler if this
 * packet is not an encoded video or an unsupported packet.
 *
 *
 * Returns: On success `1` if the packet was copied, `0` if it wasn't or a negative representing an `AVERROR` on failure.
 */
int vid_handler_copy_packet(KomputeVideoHandler *src_vid_handler, KomputeVideoHandler *dst_vid_handler);

/**
 * vid_handler_decode_frame:
 * @vid_handler: Pointer to a video handler structure.
 *
 * Sends an encoded packet hold by @vid_handler to the codec context and decodes into a frame.
 *
 *
 * Returns: On success `0` or a positive integer and negative representing an `AVERROR` on failure.
 */
int vid_handler_decode_frame(KomputeVideoHandler *vid_handler);

/**
 * vid_handler_receive_packet:
 * @vid_handler: Pointer to a video handler structure.
 *
 * Demuxes from a media container an encoded packet hold by @vid_handler.
 *
 *
 * Returns: On success `0` or a positive integer and negative representing an `AVERROR` on failure or end-of-stream.
 */
int vid_handler_receive_packet(KomputeVideoHandler *vid_handler);

/**
 * vid_handler_encode_frame:
 * @vid_handler: Pointer to a video handler structure.
 *
 * Encodes a raw frame hold by @vid_handler into a packet to be muxed to a media container.
 *
 *
 * Returns: On success `0` or a positive integer and negative representing an `AVERROR` on failure.
 */
int vid_handler_encode_frame(KomputeVideoHandler *vid_handler);

/**
 * vid_handler_submit_frame:
 * @vid_handler: Pointer to a video handler structure.
 * @dev: VkDevice handle of a logical Vulkan device.
 * @dev_mem: VkDeviceMemory handle of a logical device memory.
 * @buf_offset: Offset of the memory to map with.
 * @buf_size: Size of the memory to map with.
 *
 * Maps @buf_size bytes of @vid_handler raw frame data, offset at @buf_offset, to @dev_mem logical device memory of the
 * corresponding @dev logical device. This action corresponds to a frame upload.
 *
 *
 * Returns: On success `VK_SUCCESS` or a positive integer representing an Vulkan error on failure.
 */
int vid_handler_submit_frame(
  KomputeVideoHandler *vid_handler, VkDevice dev, VkDeviceMemory dev_mem, VkDeviceSize buf_offset, VkDeviceSize buf_size);

/**
 * vid_handler_receive_frame:
 * @vid_handler: Pointer to a video handler structure.
 * @dev: VkDevice handle of a logical Vulkan device.
 * @dev_mem: VkDeviceMemory handle of a logical device memory.
 * @buf_offset: Offset of the memory to map with.
 * @buf_size: Size of the memory to map with.
 *
 * Maps @buf_size bytes of raw RGBA32 frame data, offset at @buf_offset, to @dev_mem logical device memory of the
 * corresponding @dev logical device. This action corresponds to a frame download.
 *
 *
 * Returns: On success `VK_SUCCESS` or a positive integer representing an Vulkan error on failure.
 */
int vid_handler_receive_frame(
  KomputeVideoHandler *vid_handler, VkDevice dev, VkDeviceMemory dev_mem, VkDeviceSize buf_offset, VkDeviceSize buf_size);

/**
 * vid_handler_cleanup:
 * @vid_handler: Pointer to a video handler structure.
 *
 * Cleanups the @vid_handler video handler, closing and freeing the video components and the picture frames.
 */
void vid_handler_cleanup(KomputeVideoHandler *vid_handler);



#endif /* KOMP_VIDEO_H */

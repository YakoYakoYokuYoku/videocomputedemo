import lldb


def break_retq(debugger: lldb.SBDebugger, command: str, result: lldb.SBCommandReturnObject, internal_dict):
    target = debugger.GetSelectedTarget()
    process = target.GetProcess()
    main_thread = process.GetThreadAtIndex(0)
    curr_frame = main_thread.GetSelectedFrame()
    function = curr_frame.GetFunction()

    for inst in function.GetInstructions(target):
        if inst.GetMnemonic(target).startswith("retq"):
            target.BreakpointCreateBySBAddress(inst.GetAddress())


def __lldb_init_module(debugger: lldb.SBDebugger, internal_dict):
    debugger.HandleCommand("command script add -f " +
                           __name__ + "." + break_retq.__name__ + " bretq")

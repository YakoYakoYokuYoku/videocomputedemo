# Vulkan Video Compute

Vulkan demo of video processing with compute shaders.

![Image filtering WebM demonstration](docs/cars_side_by_side.webm)

## Description

VulkanVideoCompute is a software demo of motion pictures processing with SPIRV compute shaders.

It consists on a video pipeline that starts with a muxed input video, then by demuxing it into packets, decoding the video
packets into frames, submitting those frames to a compute pipeline, applying a filter via a compute shader, receiving the
processed images, later encoding them into packets and lastly muxing those packets into an output video.

![Pipeline graph representation](docs/video_compute_pipeline.png)

## Usage

```
vulkanvideocompute [-speed <0-10>] [-shader <path to SPIRV binary>] [-bitrate <bitrate>] <input-video>.* <output-video>.mkv

    -speed <0-10>: Speed of the rav1e encoder from 0 to 10.
    -shader <path to SPIRV binary>: Compute shader to be used (see texture.spv).
    -bitrate <bitrate>: Bitrate of the encoded video in bits, can be suffixed with k (kilos) and M (megas).

    -help: Show this help message.

    <input-video>.*: Input video with both container and codec formats supported by FFmpeg.
    <output-video>.*: Output video encoded in AV1 contained in a format that supports this codec.
```

## Building

To build the project the following requirements must be present:

- The CMake project builder.
- GCC or Clang with C99 and later support (tested) or Visual C++ 14 (not tested).
- The Vulkan loader, headers and SDK.
- The glslangValidator GLSL compiler.
- The FFmpeg libraries and headers (with the `pkg-config` files).

Optionally for debugging and tinkering with this project:

- The libheif library (with the `pkg-config` files).
- The Vulkan validations layers.

Run the following commands to get the output program of the demo (includes shader compilation).

```sh
cmake . -B build
make -C build

# or

cmake . -G Ninja -B build
ninja -C build
```

## Documentation

The documentation is distributed among the C sources of the project, although it can be built using HotDoc.

```sh
hotdoc run --config hotdoc.json
```

## Acknowledgements

- Vulkan and the Vulkan logo are trademarks of the Khronos Group Inc.

- The [libheif](https://github.com/strukturag/libheif) is distributed under the terms of the GNU Lesser General Public License and is copyright of Struktur AG.

- FFmpeg is licensed under the GNU Lesser General Public License (LGPL) version 2.1 or later. However, FFmpeg incorporates several optional parts and optimizations that are covered by the GNU General Public License (GPL) version 2 or later. If those parts get used the GPL applies to all of FFmpeg.

- The [sobel filter](https://gist.github.com/Hebali/6ebfc66106459aacee6a9fac029d0115) was authored by Patrick Hebron (@Hebali).

- The HotDoc [documentation system](https://github.com/hotdoc/hotdoc) is licensed under the GNU Lesser General Public License (LGPL) version 2.1 or later.

- The GTA V [driving video](https://www.youtube.com/watch?v=H9NrTYuomjw) is from the youtube user dann462.

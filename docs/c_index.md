---
short-description: VulkanVideoCompute C API
...

# C API reference

This the reference API for the Vulkan Compute video processor.
